package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.integrationTests.BaseIntegrationTest;
import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.AnalyticsDto;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.dao.PurchaseDAO;
import net.thumbtack.onlineshop.jdbc.model.*;
import net.thumbtack.onlineshop.services.PurchaseAnalytics;
import net.thumbtack.onlineshop.services.SessionService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class PurchaseAnalyticsUnitTest extends BaseUnitTest {

    @InjectMocks
    private PurchaseAnalytics purchaseAnalytics;
    @Mock
    private SessionService sessionServiceMock;
    @Mock
    private PurchaseDAO purchaseDAOMock;
    @Mock
    private CategoryDAO categoryDAOMock;
    @Mock
    private ProductDAO productDAOMock;

    @Test
    public void testPurchaseAnalytics() throws ServerException {
        Admin admin = getAdmin(1,1);
        admin.getUser().setStatus(UserStatus.ADMIN);
        List<Purchase> purchases = getPurchasesList();

        final Product product2 = getProduct(2);
        product2.setCategories(getSomeCategories(2,1));

        final Product product3 = getProduct(3);
        product3.setName("test");
        product3.setCategories(getSomeCategories(2,1));
        List<Product> list = new ArrayList<>();
        list.add(product2);
        list.add(product3);

        when(sessionServiceMock.getActiveUser(Mockito.anyString())).thenReturn(admin.getUser());
        when(productDAOMock.selectAllProductByCategory(1)).thenReturn(list);
        when(categoryDAOMock.selectCategoryById(Mockito.anyInt())).thenReturn(new Category());
        when(purchaseDAOMock.selectPurchasesByName("name2")).thenReturn(Arrays.asList(new Purchase[]{purchases.get(1)}));
        when(purchaseDAOMock.selectPurchasesByName("test")).thenReturn(Arrays.asList(new Purchase[]{purchases.get(2)}));

        AnalyticsDto response =  purchaseAnalytics.getPurchaseAnalytics(null, null, new int[] {1,2}, "price", "up", 0, 0, "token");

        List<Purchase> excepted = new ArrayList<>();
        excepted.add(purchases.get(2));
        excepted.add(purchases.get(1));
        assertEquals(toAnalyticsDto(excepted,300, 3),response);

    }

    private List<Purchase> getPurchasesList(){
        List<Purchase> purchases = new ArrayList<>();
        purchases.add(getPurchase(getProduct(1),1,4));

        Product product = getProduct(2);
        product.setCategories(getSomeCategories(2,1));
        product.setPrice(200);
        purchases.add(getPurchase(product, 2,2));

        product.setId(3);
        product.setName("test");
        product.setPrice(100);
        purchases.add(getPurchase(product,3,1));
        return purchases;
    }

    private AnalyticsDto toAnalyticsDto(List<Purchase> purchases, int sum, int count) {
        AnalyticsDto analyticsDto = new AnalyticsDto();
        analyticsDto.setPurchases(purchases);
        analyticsDto.setCount(count);
        analyticsDto.setGain(sum);
        return analyticsDto;
    }
}
