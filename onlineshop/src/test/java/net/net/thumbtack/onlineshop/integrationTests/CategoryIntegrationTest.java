package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.assertEquals;

public class CategoryIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testInsertCategory() {
        saveAdmin();
        Category category1 = getCategory(1);
        Category category2 = getCategory(2);
        CategoryDto categoryDto1 = toCategoryDto(category1);
        CategoryDto categoryDto2 = toCategoryDto(category2);
        categoryDto2.setParentId(1);

        CategoryDto response1 = restTemplate.postForObject(rootUrl+"categories",categoryDto1, CategoryDto.class);
        CategoryDto response2 = restTemplate.postForObject(rootUrl+"categories",categoryDto2, CategoryDto.class);

        assertEquals(categoryDto1, response1);
        assertEquals(categoryDto2, response2);
    }

    @Test
    public void testUpdateCategory() {
        saveAdmin();
        saveCategory(1);
        CategoryDto categoryDto = saveCategory(2);
        categoryDto.setParentId(1);
        categoryDto.setName("newName");

        restTemplate.put(rootUrl + "categories/2", categoryDto);

        ResponseEntity<CategoryDto[]> responseEntity = restTemplate
                .getForEntity(rootUrl + "categories",CategoryDto[].class);
        assertEquals(categoryDto, responseEntity.getBody()[1]);

    }

    @Test
    public void testDeleteCategory() {
        saveAdmin();
        saveCategory(1);
        restTemplate.delete(rootUrl+"categories/1");
        assertEquals(0, restTemplate.getForEntity(rootUrl + "categories", CategoryDto[].class).getBody().length);

    }

    private CategoryDto saveCategory(int id){
        CategoryDto categoryDto = toCategoryDto(getCategory(id));
        return restTemplate.postForObject(rootUrl+"categories",categoryDto, CategoryDto.class);
    }

    private UserDto saveAdmin(){
        UserDto userDto = getUserDto( getAdmin(1,1));
        ResponseEntity<UserDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);
        return userDto;
    }

}
