package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.PurchaseDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.PurchaseMapper;
import net.thumbtack.onlineshop.jdbc.model.Purchase;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PurchaseDaoTest extends BaseUnitTest {
    @InjectMocks
    private PurchaseDAO purchaseDAO;
    @Mock
    private DataSource dataSourceMock;
    @Mock
    private PurchaseMapper purchaseMapperMock;
    @Mock
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    private JdbcTemplate jdbcTemplateMock;

    @Test
    public void testInsertPurchase() {
        Purchase purchase1 =getPurchase(getProduct(1),1,2);
        when(namedParameterJdbcTemplateMock.update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Map<String, Object> keyMap = new HashMap<String, Object>();
                keyMap.put("",1);
                ((GeneratedKeyHolder)args[2]).getKeyList().add(keyMap);
                return 1;
            }
        });

        Purchase response = purchaseDAO.insertPurchase(purchase1);
        assertEquals(purchase1, response);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class));
    }

    @Test
    public void testSelectPurchasesByClient() {
        List<Purchase> purchases = new ArrayList<>();
        purchases.add(getPurchase(getProduct(1),2,5));
        when(jdbcTemplateMock.query(purchaseDAO.SQL_SELECT_ALL_BY_CLIENT, new Object[]{2}, purchaseMapperMock)).thenReturn(purchases);
        Purchase purchase = purchaseDAO.selectPurchasesByClient(2).get(0);
        assertEquals(purchases.get(0), purchase );
        verify(jdbcTemplateMock, times(1))
                .query(purchaseDAO.SQL_SELECT_ALL_BY_CLIENT, new Object[]{2}, purchaseMapperMock);
    }

    @Test
    public void testSelectPurchasesByName(){
        List<Purchase> purchases = new ArrayList<>();
        purchases.add(getPurchase(getProduct(1),2,5));
        when(jdbcTemplateMock.query(purchaseDAO.SQL_SELECT_ALL_BY_NAME, new Object[]{"name1"}, purchaseMapperMock)).thenReturn(purchases);
        Purchase purchase = purchaseDAO.selectPurchasesByName("name1").get(0);
        assertEquals(purchases.get(0), purchase );
        verify(jdbcTemplateMock, times(1))
                .query(purchaseDAO.SQL_SELECT_ALL_BY_NAME, new Object[]{"name1"}, purchaseMapperMock);
    }

    @Test
    public void testSelectAllPurchases(){
        List<Purchase> purchases = new ArrayList<>();
        purchases.add(getPurchase(getProduct(1),2,5));
        purchases.add(getPurchase(getProduct(2),3,3));
        when(jdbcTemplateMock.query(purchaseDAO.SQL_SELECT_ALL, purchaseMapperMock)).thenReturn(purchases);
        List<Purchase> response = purchaseDAO.selectAllPurchases();
        assertEquals(purchases, response);
        verify(jdbcTemplateMock, times(1))
                .query(purchaseDAO.SQL_SELECT_ALL, purchaseMapperMock);
    }

}
