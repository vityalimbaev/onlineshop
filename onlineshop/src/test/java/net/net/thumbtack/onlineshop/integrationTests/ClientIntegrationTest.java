package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.Client;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClientIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testSaveClient() throws ServerException {
        Client client = getClient(1,1);
        UserDto userDto = getUserDto(client);
        userDto.setPhone("89876785432");

        UserDto response = restTemplate.postForObject(rootUrl +"clients", userDto,UserDto.class);

        userDto.setId(1);
        userDto.setPassword(null);
        assertEquals(userDto, response);
    }

    @Test
    public void updateClient() throws ServerException {
        Client client =getClient(1,1);
        client.setPhone("89874563421");
        saveClient(client);
        UserDto userDto = getUserDto(client);
        userDto.setNewPassword("newPassword");
        userDto.setOldPassword("password");
        userDto.setFirstName("новоеИмя");

        restTemplate.put(rootUrl+"clients", userDto, UserDto.class);

        userDto.setNewPassword(null);
        userDto.setOldPassword(null);
        userDto.setPassword(null);
        userDto.setId(1);
        userDto.setDeposit(0);
        assertEquals(userDto, restTemplate.getForObject(rootUrl+"clients",UserDto.class));

    }

    @Test
    public void testPutCash() throws ServerException {
        Client client = getClient(1,1);
        client.setPhone("89879878709");
        saveClient(client);

        Integer cash = 60000;
        UserDto request = new UserDto();
        request.setDeposit(cash);

        restTemplate.postForObject(rootUrl+"deposits", request, UserDto.class);

        UserDto response = getUserDto(client);
        response.setPassword(null);
        response.setId(1);
        response.setDeposit(60000);
        assertEquals(response, restTemplate.getForObject(rootUrl+"clients",UserDto.class));
    }

    private void saveClient(Client client){
        UserDto userDto = getUserDto(client);
        restTemplate.postForEntity(rootUrl +"clients", userDto, UserDto.class);
    }

}
