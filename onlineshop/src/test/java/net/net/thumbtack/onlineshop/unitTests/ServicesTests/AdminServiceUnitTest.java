package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import net.thumbtack.onlineshop.services.AdminService;
import net.thumbtack.onlineshop.services.SessionService;
import net.thumbtack.onlineshop.services.UserService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class AdminServiceUnitTest extends BaseUnitTest {
    @InjectMocks
    protected AdminService adminService;
    @Mock
    private UserService userServiceMock;
    @Mock
    private AdminDAO adminDAO;
    @Mock
    private SessionService sessionService;


    @Test
    public void testSaveAdminDao() throws ServerException {

        Admin admin = getAdmin(1, 1);
        admin.setId(0);
        admin.getUser().setId(0);
        UserDto userDto = getUserDto(admin);
        userDto.setPassword(null);

        when(userServiceMock.saveUser(userDto)).thenReturn(admin.getUser());
        when(adminDAO.insertAdmin(admin)).thenReturn(admin);
        when(sessionService.setActiveUser(admin.getUser())).thenReturn("token");

        UserDto responseUserDto1 = adminService.saveAdmin(userDto);
        userDto.setStatus(null);
        assertEquals(userDto, responseUserDto1);

        userDto.setId(1);
        verify(userServiceMock, times(1)).saveUser(userDto);
        verify(adminDAO, times(1)).insertAdmin(admin);
        verify(sessionService, times(1)).setActiveUser(admin.getUser());

        try {
            userDto.setPosition(null);
            adminService.saveAdmin(userDto);
            fail();
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_NULL_POSITION, ex.serverErrorCode);
        }

    }
    @Test
    public void updateAdmin() throws ServerException {

        Admin admin = getAdmin(1, 1);
        UserDto userDto = getUserDto(admin);
        userDto.setOldPassword(admin.getUser().getPassword());
        userDto.setNewPassword("newPassword");
        userDto.setStatus(null);

        String token = "token";
        when(sessionService.getActiveUser(token)).thenReturn(admin.getUser());
        when(userServiceMock.updateUser(admin.getUser(), userDto)).thenReturn(admin.getUser());

        UserDto response = adminService.updateAdmin(userDto, token);


        userDto.setNewPassword(null);
        userDto.setPassword(null);
        userDto.setOldPassword(null);
        assertEquals(userDto, response);

        userDto.setOldPassword(admin.getUser().getPassword()+"other");
        userDto.setNewPassword("testPassword");

        try {
            adminService.updateAdmin(userDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_INVALID_PASSWORD, ex.getErrorCode());
        }

        try {
            userDto.setPosition(null);
            adminService.saveAdmin(userDto);
            fail();
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_NULL_POSITION, ex.serverErrorCode);
        }
    }

}
