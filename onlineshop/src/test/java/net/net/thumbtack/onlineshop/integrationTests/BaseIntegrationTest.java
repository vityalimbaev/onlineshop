package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.*;
import org.junit.After;
import org.junit.Before;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

public class BaseIntegrationTest {

    String rootUrl ="http://localhost:8080/api/";
    RestTemplate restTemplate ;

    @Before
    public void setUp() {
        restTemplate = new RestTemplate();
        List<ClientHttpRequestInterceptor> list = new ArrayList<ClientHttpRequestInterceptor>(){{
            add(new StatefulRestTemplateInterceptor());
        }};
        restTemplate.setInterceptors(list);

        restTemplate.postForObject(rootUrl + ("debug/clear"), null, Void.class);
    }

    @After
    public void logout() {
        restTemplate.delete(rootUrl + "sessions");
    }


    protected User getUser(int userId) {
        User user = new User();
        user.setId(userId);
        user.setLogin("login" + userId);
        user.setPassword("password");
        user.setFirstName("имя");
        user.setSecondName("имя");
        user.setThirdName("имя");
        return user;
    }

    Admin getAdmin(int userId, int adminId) {
        Admin admin = new Admin();
        admin.setUser(getUser(userId));
        admin.setPosition("position");
        admin.setId(adminId);
        return admin;
    }

    Client getClient(int userId, int clientId){
        Client client = new Client();
        client.setId(clientId);
        client.setUser(getUser(userId));
        client.setDeposit(500000);
        client.setEmail("email@gmail.ru");
        client.setAddress("address");
        client.setPhone("phone");
        return client;
    }

    UserDto getUserDto(Admin admin) {
        UserDto UserDto = new UserDto();
        UserDto.setFirstName(admin.getUser().getFirstName());
        UserDto.setSecondName(admin.getUser().getSecondName());
        UserDto.setThirdName(admin.getUser().getThirdName());
        UserDto.setPosition(admin.getPosition());
        UserDto.setLogin(admin.getUser().getLogin());
        UserDto.setPassword(admin.getUser().getPassword());
        return UserDto;
    }

    UserDto getUserDto(Client client) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(client.getUser().getFirstName());
        userDto.setSecondName(client.getUser().getSecondName());
        userDto.setThirdName(client.getUser().getThirdName());
        userDto.setLogin(client.getUser().getLogin());
        userDto.setPassword(client.getUser().getPassword());
        userDto.setEmail(client.getEmail());
        userDto.setPhone(client.getPhone());
        userDto.setAddress(client.getAddress());
        return userDto;
    }

    public Product getProduct(int productID) {
        Product product = new Product();
        product.setId(productID);
        product.setName("name" + productID);
        product.setCount(5);
        product.setPrice(250);
        return product;
    }

    public Category getCategory(int categoryID) {
        Category category = new Category();
        category.setId(categoryID);
        category.setName("name" + categoryID);

        return category;
    }

    public CategoryDto toCategoryDto(Category category){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(category.getName());
        categoryDto.setId(category.getId());
        return categoryDto;
    }

    public ProductDto toProductDto(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setCount(product.getCount());
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice());

        List<Category> list = product.getCategories();
        List<Integer> listIds = new ArrayList<>();

        if(list != null) {
            for (Category category : list) {
                listIds.add(category.getId());
            }
        }
        productDto.setCategoriesId(listIds);

        return productDto;
    }
}
