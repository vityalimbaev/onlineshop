package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.BasketDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.jdbc.dao.BasketDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.*;
import net.thumbtack.onlineshop.services.BasketService;
import net.thumbtack.onlineshop.services.SessionService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class BasketServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private BasketService basketService;

    @Mock
    private BasketDAO basketDAOMock;

    @Mock
    private SessionService sessionServiceMock;

    @Mock
    private ClientDAO clientDAOMock;

    @Mock
    private ProductDAO productDAOMOck;

    @Test
    public void testSavePosition() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);
        ProductDto productDto = toProductDto(getProduct(1));

        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(productDAOMOck.selectProductByID(productDto.getId())).thenReturn(getProduct(1));
        when(clientDAOMock.selectClientByLogin(client.getUser().getLogin())).thenReturn(client);

        Basket excepted = new Basket();
        excepted.addProduct(getProduct(1), productDto.getCount());

        assertEquals(toBasketDto(excepted), basketService.savePosition(productDto, "token"));

        try {
            productDto.setId(1);
            productDto.setName("test");
            basketService.savePosition(productDto, "token");
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_INCORRECT_PRODUCT_REQUEST, ex.getErrorCode());
        }

        try {
            client.getUser().setStatus(UserStatus.ADMIN);
            productDto.setId(1);
            productDto.setName("name1");
            basketService.savePosition(productDto, "token");
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        verify(sessionServiceMock, times(3)).getActiveUser(Mockito.anyString());
        verify(productDAOMOck, times(2)).selectProductByID(Mockito.anyInt());
        verify(clientDAOMock, times(2)).selectClientByLogin(Mockito.anyString());
    }

    @Test
    public void testUpdatePosition() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);
        ProductDto productDto = toProductDto(getProduct(1));

        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(productDAOMOck.selectProductByID(productDto.getId())).thenReturn(getProduct(1));
        when(clientDAOMock.selectClientByLogin(client.getUser().getLogin())).thenReturn(client);

        Basket excepted = new Basket();
        excepted.addProduct(getProduct(1), productDto.getCount());

        assertEquals(toBasketDto(excepted), basketService.updatePosition(productDto, "token"));

        try {
            productDto.setId(1);
            productDto.setName("test");
            basketService.savePosition(productDto, "token");
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_INCORRECT_PRODUCT_REQUEST, ex.getErrorCode());
        }

        try {
            client.getUser().setStatus(UserStatus.ADMIN);
            productDto.setId(1);
            productDto.setName("name1");
            basketService.savePosition(productDto, "token");
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        verify(sessionServiceMock, times(3)).getActiveUser(Mockito.anyString());
        verify(productDAOMOck, times(2)).selectProductByID(Mockito.anyInt());
        verify(clientDAOMock, times(2)).selectClientByLogin(Mockito.anyString());
    }

    @Test
    public void testDeletePosition() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(clientDAOMock.selectClientByLogin(client.getUser().getLogin())).thenReturn(client);

        basketService.deletePosition(1, "token");

        try {
            client.getUser().setStatus(UserStatus.ADMIN);
            basketService.deletePosition(1, "token");
        } catch (ServerException ex) {
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        verify(sessionServiceMock, times(2)).getActiveUser(Mockito.anyString());
        verify(clientDAOMock, times(1)).selectClientByLogin(Mockito.anyString());
    }

    @Test
    public void testGetBasket() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);
        Basket basket = new Basket();
        basket.addProduct(getProduct(1), 2);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(clientDAOMock.selectClientByLogin(client.getUser().getLogin())).thenReturn(client);
        when(basketDAOMock.selectBasket(client)).thenReturn(basket);

        BasketDto basketDto = basketService.getBasket("token");
        assertEquals(toBasketDto(basket), basketDto);

        try{
            client.getUser().setStatus(UserStatus.ADMIN);
            basketService.getBasket("token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }
        verify(sessionServiceMock, times(2)).getActiveUser(Mockito.anyString());
        verify(clientDAOMock, times(1)).selectClientByLogin(Mockito.anyString());
        verify(basketDAOMock, times(1)).selectBasket(Mockito.any(Client.class));
    }

    public BasketDto toBasketDto(Basket basket) {
        BasketDto basketDto = new BasketDto();

        for (Product product : basket.getListProducts()) {
            ProductDto productDto = new ProductDto();
            productDto.setCount(product.getCount());
            productDto.setName(product.getName());
            productDto.setPrice(product.getPrice());
            productDto.setId(product.getId());
            basketDto.addPurchaseDto(productDto);
        }
        return basketDto;
    }
}
