package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.dto.UserLoginDto;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SessionIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testSessionLogin() {

        UserDto userDto = saveAdmin();

        UserLoginDto userLoginDto = new UserLoginDto();
        userLoginDto.setPassword(userDto.getPassword());
        userLoginDto.setLogin(userDto.getLogin());
        UserDto response = restTemplate.postForObject(rootUrl+"sessions", userLoginDto, UserDto.class );

        userDto.setPassword(null);
        assertEquals(userDto, response);
    }

    @Test(expected = HttpClientErrorException.class)
    public void testSessionLogout(){
        UserDto userDto = saveAdmin();
        restTemplate.delete(rootUrl+"sessions");
        restTemplate.put(rootUrl+"admin", userDto);
    }

    private UserDto saveAdmin(){
        UserDto userDto = getUserDto( getAdmin(1,1));
        ResponseEntity<UserDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);
        logout();
        return userDto;
    }


}
