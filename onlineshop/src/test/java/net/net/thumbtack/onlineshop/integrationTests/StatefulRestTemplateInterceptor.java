package net.net.thumbtack.onlineshop.integrationTests;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.List;

public class StatefulRestTemplateInterceptor implements ClientHttpRequestInterceptor {

    private static String sc;
    private String cookie;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        if (cookie != null) {
            request.getHeaders().add(HttpHeaders.COOKIE, cookie);
        }
        ClientHttpResponse response = execution.execute(request, body);
        String newCookie = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        if(newCookie != null) {
            cookie = newCookie;
            sc = cookie;
        }

        return response;
    }

    public static String getCookie() {
        return sc;
    }
}
