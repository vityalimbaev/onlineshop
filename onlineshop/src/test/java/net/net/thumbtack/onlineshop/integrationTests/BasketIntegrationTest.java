package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.BasketDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.*;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

public class BasketIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testSavePosition() throws ServerException {
       ProductDto productDto = toProductDto(getProduct(1));
       saveProduct(productDto);
       saveClient(getClient(2,1));
       productDto.setCategoriesId(null);
       BasketDto excepted =new BasketDto();
       excepted.addPurchaseDto(productDto);
       BasketDto response = restTemplate.postForObject(rootUrl+"baskets", productDto, BasketDto.class);

       assertEquals(excepted, response);

    }

    @Test
    public void testUpdatePosition() throws ServerException {
        ProductDto productDto = toProductDto(getProduct(1));
        saveProduct(productDto);
        saveClient(getClient(2,1));
        productDto.setCount(2);
        saveBasket(productDto);

        productDto.setCategoriesId(null);
        BasketDto excepted =new BasketDto();
        excepted.addPurchaseDto(productDto);

        restTemplate.put(rootUrl+"baskets", productDto, BasketDto.class);

        assertEquals(excepted, restTemplate.getForObject(rootUrl+"baskets", BasketDto.class));
    }

    @Test
    public void deletePosition() {
        ProductDto productDto = toProductDto(getProduct(1));
        saveProduct(productDto);
        saveClient(getClient(2,1));
        productDto.setCount(2);
        saveBasket(productDto);

        restTemplate.delete(rootUrl+"baskets/1");
        BasketDto excepted = new BasketDto();
        excepted.setRemaining(new ArrayList<ProductDto>());
        assertEquals(excepted, restTemplate.getForObject(rootUrl + "baskets", BasketDto.class));


    }

    private void saveClient(Client client){
        UserDto userDto = getUserDto(client);
        userDto.setPhone("89875673412");
        restTemplate.postForEntity(rootUrl +"clients", userDto, UserDto.class);
        userDto.setDeposit(500);
        restTemplate.postForObject(rootUrl+"deposits",userDto, UserDto.class);
    }

    private ProductDto saveProduct(ProductDto productDto) {
        UserDto userDto = getUserDto( getAdmin(1,1));
        userDto.setStatus(UserStatus.ADMIN);
        restTemplate.postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);

        ResponseEntity<ProductDto> responseEntity= restTemplate.postForEntity(rootUrl + "products", productDto, ProductDto.class);
        logout();
        return responseEntity.getBody();
    }

    private void saveBasket(ProductDto productDto){
        BasketDto response = restTemplate.postForObject(rootUrl+"baskets", productDto, BasketDto.class);
    }

}
