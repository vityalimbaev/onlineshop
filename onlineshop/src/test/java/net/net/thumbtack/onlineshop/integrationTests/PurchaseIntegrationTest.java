package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.config.SpringConfig;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.PurchaseDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;

public class PurchaseIntegrationTest extends BaseIntegrationTest {



    @Test
    public void testPurchase(){
        Client client = getClient(2,1);
        Product product = getProduct(1);
        ProductDto productDto =toProductDto(product);
        saveProduct(productDto);
        saveClient(client);

        ProductDto request = new ProductDto();
        request.setPrice(product.getPrice());
        request.setName(product.getName());
        request.setCount(1);
        request.setId(product.getId());

        ProductDto response = restTemplate.postForObject(rootUrl+"purchases", request,ProductDto.class);
        assertEquals(request, response);
    }

    private void saveClient(Client client){
        UserDto userDto = getUserDto(client);
        userDto.setPhone("89876543415");
        restTemplate.postForEntity(rootUrl +"clients", userDto, UserDto.class);
        userDto.setDeposit(500);
        restTemplate.postForObject(rootUrl+"deposits",userDto, UserDto.class);
    }

    private ProductDto saveProduct(ProductDto productDto) {
        UserDto userDto = getUserDto( getAdmin(1,1));
        restTemplate.postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);

        ResponseEntity<ProductDto> responseEntity= restTemplate.postForEntity(rootUrl + "products", productDto, ProductDto.class);
        logout();
        return responseEntity.getBody();
    }
}
