package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.dao.PurchaseDAO;
import net.thumbtack.onlineshop.jdbc.model.*;
import net.thumbtack.onlineshop.services.PurchaseService;
import net.thumbtack.onlineshop.services.SessionService;
import net.thumbtack.onlineshop.services.UserService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PurchaseServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private PurchaseService purchaseServiceMock;
    @Mock
    private UserService userServiceMock;
    @Mock
    private PurchaseDAO purchaseDAOMock;
    @Mock
    private SessionService sessionServiceMock;
    @Mock
    private ClientDAO clientDAOMock;
    @Mock
    private ProductDAO productDAOMock;


    @Test
    public void purchases() throws ServerException {

        Client client = getClient(1,1);
        client.getUser().setStatus(UserStatus.CLIENT);
        Product product = getProduct(1);
        ProductDto productDto = new ProductDto();
        productDto.setId(1);
        productDto.setName(product.getName());
        productDto.setCount(3);
        productDto.setPrice(250);

        Purchase purchase = new Purchase();
        purchase.setProductId(product.getId());
        purchase.setName(product.getName());
        purchase.setPrice(product.getPrice());
        purchase.setCount(product.getCount());
        purchase.setClientId(client.getId());

        when(clientDAOMock.selectClientByLogin(client.getUser().getLogin())).thenReturn(client);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(productDAOMock.selectProductByID(product.getId())).thenReturn(product);

        assertEquals(toProductDto(purchase), purchaseServiceMock.purchaseProduct(productDto,"token"));
        verify(clientDAOMock,times(1)).selectClientByLogin(client.getUser().getLogin());
        verify(sessionServiceMock, times(1)).getActiveUser("token");
        verify(productDAOMock, times(2)).selectProductByID(productDto.getId());
        verify(clientDAOMock, times(1)).updateDeposit(500000-750, client.getId());
        verify(purchaseDAOMock, times(1)).insertPurchase(purchase);
        verify(productDAOMock,times(1)).updateProduct(product);
    }

    public ProductDto toProductDto(Purchase purchase) {
        ProductDto productDto = new ProductDto();
        productDto.setCount(purchase.getCount());
        productDto.setName(purchase.getName());
        productDto.setId(purchase.getProductId());
        productDto.setPrice(purchase.getPrice());
        return productDto;
    }
}
