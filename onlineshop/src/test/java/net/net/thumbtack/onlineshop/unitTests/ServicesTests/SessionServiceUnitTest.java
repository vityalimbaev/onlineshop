package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserLoginDto;
import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.services.SessionService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class SessionServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private SessionService sessionServiceMock;
    @Mock
    private UserDAO userDAOMock;


    @Test
    public void testLogin() throws  ServerException {
        User user = getUser(1);

        UserLoginDto userLoginDto = new UserLoginDto();
        userLoginDto.setLogin(user.getLogin());
        userLoginDto.setPassword(user.getPassword());

        when(userDAOMock.selectUserByLogin(userLoginDto.getLogin())).thenReturn(user);

        assertEquals(36, sessionServiceMock.login(userLoginDto).length());
    }


    @Test
    public void testSetActiveUser( ) throws IOException {
        User user =getUser(1);
        assertEquals( 36,sessionServiceMock.setActiveUser(user).length());
    }

    @Test(expected = ServerException.class)
    public void testGetActiveUser() throws ServerException {
        sessionServiceMock.getActiveUser("token");
    }


}
