package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import com.sun.security.ntlm.Server;
import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import net.thumbtack.onlineshop.services.UserService;
import net.thumbtack.onlineshop.services.ValidationService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private UserService userServiceMock;
    @Mock
    private UserDAO userDAOMock;
    @Mock
    private ValidationService validationServiceMock;

    @Test
    public void testSaveUser() throws ServerException {

        User user = getUser(1);
        user.setId(null);
        User newUser = getUser(1);
        user.setStatus(UserStatus.ADMIN);
        UserDto userDto = getUserDto(getAdmin(1,1));
        when(userDAOMock.insertUser(user)).thenReturn(newUser);

        assertEquals(newUser, userServiceMock.saveUser(userDto));

        verify(validationServiceMock,times(1))
                .validateName(user.getFirstName(),user.getSecondName(),user.getThirdName());
        verify(validationServiceMock,times(1)).validateLogin(user.getLogin());
        verify(validationServiceMock,times(1)).validatePassword(user.getPassword());
        verify(userDAOMock,times(1)).insertUser(user);

        try {
            userDto.setFirstName(null);
            userServiceMock.saveUser(userDto);
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_NULL_NAME, ex.getErrorCode());
        }
    }

    @Test
    public void testUpdateUser() throws ServerException {

        User user = getUser(1);
        user.setId(0);
        user.setStatus(UserStatus.ADMIN);
        UserDto userDto = getUserDto(getAdmin(1,1));
        when(userDAOMock.updateUser(user)).thenReturn(user);

        assertEquals(user, userServiceMock.updateUser(user, userDto));

        verify(validationServiceMock,times(1))
                .validateName(user.getFirstName(),user.getSecondName(),user.getThirdName());
        verify(userDAOMock,times(1)).updateUser(user);

    }
}
