package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import net.thumbtack.onlineshop.services.ClientService;
import net.thumbtack.onlineshop.services.SessionService;
import net.thumbtack.onlineshop.services.UserService;
import net.thumbtack.onlineshop.services.ValidationService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ClientServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private ClientService clientServiceMock;

    @Mock
    private SessionService sessionServiceMock;

    @Mock
    private ClientDAO clientDAOMock;

    @Mock
    private UserService userServiceMock;

    @Mock
    private ValidationService validationService;

    @Test
    public void testSaveClient() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);
        client.setId(null);
        client.setDeposit(null);
        client.setBasket(null);
        UserDto userDto = getUserDto(client);
        userDto.setDeposit(null);
        userDto.setId(null);

        when(userServiceMock.saveUser(Mockito.any(UserDto.class))).thenReturn(client.getUser());
        when(clientDAOMock.insertClient(Mockito.any(Client.class))).thenReturn(client);

        UserDto responseUserDto = clientServiceMock.saveClient(userDto);
        userDto.setStatus(null);
        userDto.setPassword(null);
        assertEquals(userDto, responseUserDto);

        verify(clientDAOMock, times(1)).insertClient(client);
        verify(userServiceMock, times(1)).saveUser(userDto);
        verify(sessionServiceMock, times(1)).setActiveUser(client.getUser());

    }

    @Test
    public void testUpdateClient() throws ServerException {
        Client client = getClient(1, 1);
        client.getUser().setStatus(UserStatus.CLIENT);
        client.setId(null);
        client.setDeposit(null);
        client.setBasket(null);
        UserDto userDto = getUserDto(client);
        userDto.setDeposit(null);
        userDto.setId(null);
        userDto.setOldPassword("password");
        userDto.setNewPassword("newPassword");

        when(sessionServiceMock.getActiveUser("token")).thenReturn(client.getUser());
        when(userServiceMock.updateUser(client.getUser(),userDto)).thenReturn(client.getUser());
        when(clientDAOMock.updateClient(Mockito.any(Client.class))).thenReturn(client);

        UserDto responseUserDto = clientServiceMock.updateClient(userDto, "token");
        userDto.setStatus(null);
        userDto.setPassword(null);
        userDto.setOldPassword(null);
        userDto.setNewPassword(null);
        assertEquals(userDto, responseUserDto);

        verify(clientDAOMock, times(1)).updateClient(Mockito.any(Client.class));
        verify(userServiceMock, times(1)).updateUser(Mockito.any(User.class),Mockito.any(UserDto.class));
        verify(sessionServiceMock, times(1)).getActiveUser(Mockito.anyString());

        try {
            userDto.setOldPassword("otherPassword");
            clientServiceMock.updateClient(userDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_INVALID_PASSWORD, ex.getErrorCode());
        }
    }

    @Test
    public void testUpdateDeposit() throws ServerException {
        Client client = getClient(1,1);
        UserDto userDto = getUserDto(client);
        Integer deposit = 500000;
        Integer cash = 100000;

        when(sessionServiceMock.getProfileByToken("token")).thenReturn(userDto);
        when(clientDAOMock.updateDeposit(deposit+cash,client.getId())).thenReturn(deposit);

        Integer deposit1 = 600000;
        client.setDeposit(deposit1);
        UserDto userDto1 = getUserDto(client);
        userDto1.setPassword(null);
        userDto1.setDeposit(deposit1);

        assertEquals(userDto1, clientServiceMock.putCash(cash,"token"));

        try {
            cash =-1;
            clientServiceMock.putCash(cash,"token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_INCORRECT_DEPOSIT, ex.getErrorCode());
        }
        verify(clientDAOMock, times(1)).updateDeposit(Mockito.anyInt(), Mockito.anyInt());
        verify(sessionServiceMock, times(1)).getProfileByToken("token");
    }

    @Test
    public void testSelectAllClient() throws ServerException {

        List<UserDto> excepted =new ArrayList<>();
        List<Client> lists = getSomeClients(2);
        for(Client client:lists){
            UserDto userDto = getUserDto(client);
            userDto.setStatus(UserStatus.CLIENT);
            userDto.setPassword(null);
            excepted.add(userDto);
        }
        when(sessionServiceMock.getActiveUser("token")).thenReturn(new User());
        when(clientDAOMock.selectAllClient()).thenReturn(lists);


        assertEquals(excepted, clientServiceMock.getAllClient("token"));

        verify(clientDAOMock, times(1)).selectAllClient();
        verify(sessionServiceMock, times(1)).getActiveUser("token");
    }

}
