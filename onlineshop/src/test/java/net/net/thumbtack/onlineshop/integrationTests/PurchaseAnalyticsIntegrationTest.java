package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.dto.*;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.Purchase;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PurchaseAnalyticsIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testGetPurchaseAnalytics(){
        final CategoryDto categoryDto = saveCategory(1);

        List<ProductDto> products = new ArrayList<>();
        Product product = getProduct(1);
        product.setPrice(200);
        product.setCategories(new ArrayList<Category>(){{
            add(fromCategoryDto(categoryDto));
        }});
        products.add(toProductDto(product));

        product = getProduct(2);
        product.setPrice(100);
        product.setCount(1);
        product.setCategories(new ArrayList<Category>(){{
            add(fromCategoryDto(categoryDto));
        }});
        products.add(toProductDto(product));

        product = getProduct(3);
        product.setPrice(300);
        product.setCount(2);
        products.add(toProductDto(product));
        saveProducts(products);

        savePurchases(products);
        AnalyticsDto excepted = new AnalyticsDto();
        excepted.setGain(300);
        excepted.setCount(6);
        products.remove(2);
        List<Purchase> pur = getPurchase(products);
        Collections.reverse(pur);
        excepted.setPurchases(pur);

        UserLoginDto userLoginDto = new UserLoginDto();
        userLoginDto.setLogin("login1");
        userLoginDto.setPassword("password");
        restTemplate.postForEntity(rootUrl + "sessions", userLoginDto, UserDto.class);

        AnalyticsDto response = restTemplate.getForObject(rootUrl+"purchases?sort=price&categories=1",AnalyticsDto.class);
        assertEquals(excepted, response );
    }

    private List<ProductDto> saveProducts(List<ProductDto> productDto) {
        UserLoginDto userLoginDto = new UserLoginDto();
        userLoginDto.setLogin("login1");
        userLoginDto.setPassword("password");
        restTemplate.postForEntity(rootUrl + "sessions", userLoginDto, UserDto.class);

        ResponseEntity<ProductDto> responseEntity1 = restTemplate.postForEntity(rootUrl + "products", productDto.get(0), ProductDto.class);
        ResponseEntity<ProductDto> responseEntity2 = restTemplate.postForEntity(rootUrl + "products", productDto.get(1), ProductDto.class);
        ResponseEntity<ProductDto> responseEntity3 = restTemplate.postForEntity(rootUrl + "products", productDto.get(2), ProductDto.class);

        List<ProductDto> list = new ArrayList<>();
        list.add(responseEntity1.getBody());
        list.add(responseEntity2.getBody());
        list.add(responseEntity3.getBody());
        logout();
        return list;
    }

    private List<ProductDto> savePurchases(List<ProductDto> listProductDto){
        Client client = getClient(2,1);
        client.setPhone("89876542314");
        UserDto userDto = getUserDto(client);
        userDto.setId(2);
        userDto.setDeposit(2000);
        restTemplate.postForEntity(rootUrl + "clients", userDto, UserDto.class);
        restTemplate.postForObject(rootUrl+"deposits", userDto, UserDto.class);

        List<ProductDto> response = new ArrayList<>();
        for(ProductDto productDto:listProductDto){
            response.add(restTemplate.postForObject(rootUrl+"purchases", productDto,ProductDto.class));
        }
        logout();
        return response;
    }

    private CategoryDto saveCategory(int id){
        UserDto userDto = getUserDto( getAdmin(1,1));
        restTemplate.postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);
        CategoryDto categoryDto = toCategoryDto(getCategory(id));
        categoryDto = restTemplate.postForObject(rootUrl+"categories",categoryDto, CategoryDto.class);
        logout();
        return categoryDto;
    }

    public Category fromCategoryDto(CategoryDto categoryDto) {
        Category category = new Category();
        category.setId(categoryDto.getId());
        category.setParentId(categoryDto.getParentId());
        category.setName(categoryDto.getName());
        return category;
    }

    public List<Purchase> getPurchase(List<ProductDto>productDtos){
        List<Purchase> purchases = new ArrayList<>();
        int i = 1;
        for(ProductDto productDto : productDtos){
            Purchase purchase = new Purchase();
            purchase.setProductId(productDto.getId());
            purchase.setClientId(1);
            purchase.setPrice(productDto.getPrice());
            purchase.setCount(productDto.getCount());
            purchase.setName(productDto.getName());
            purchase.setId(i);
            i++;
            purchases.add(purchase);
        }
        return purchases;
    }

}
