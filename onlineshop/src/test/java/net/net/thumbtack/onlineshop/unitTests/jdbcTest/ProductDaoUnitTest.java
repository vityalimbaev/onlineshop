package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.ProductMapper;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductDaoUnitTest extends BaseUnitTest {
    @InjectMocks
    private ProductDAO productDAO;
    @Mock
    DataSource dataSourceMock;
    @Mock
    private ProductMapper productMapperMock;
    @Mock
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    private JdbcTemplate jdbcTemplateMock;

    @Test
    public  void testInsertProductDao(){
        Product product = getProduct(1);
        Mockito.when(namedParameterJdbcTemplateMock.update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Map<String, Object> keyMap = new HashMap<String, Object>();
                keyMap.put("",1);
                ((GeneratedKeyHolder)args[2]).getKeyList().add(keyMap);
                return 1;
            }
        });

        Product productNew = productDAO.insertProduct(product);
        assertEquals(product, productNew);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class));
    }

    @Test
    public void testUpdateProductDao(){
        Product product = getProduct(1);

        when(namedParameterJdbcTemplateMock.update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class))).thenReturn(0);
        Product productNew = productDAO.updateProduct(product);
        assertEquals(product, productNew);
    }

    @Test
    public void testDeleteProduct() {
        Product product = getProduct(1);

        when(jdbcTemplateMock.update(productDAO.SQL_DELETE,1)).thenReturn(0);
        int newProduct = productDAO.deleteProduct(product.getId());
        assertEquals(1, newProduct);

        verify(jdbcTemplateMock,times(1)).update(productDAO.SQL_DELETE,1);

    }

    @Test
    public void testSelectAllProduct() {
        Product product = getProduct(1);

        List<List<Product>> products = new ArrayList<>();
        products.add(new ArrayList<Product>());
        products.get(0).add(product);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_ALL, productMapperMock)).thenReturn(products);

        List<Product> newList = productDAO.selectAllProduct();
        assertEquals(products.get(0), newList);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_ALL, productMapperMock);
    }

    @Test
    public void testSelectProduct() {
        List<List<Product>> products = new ArrayList<>();
        products.add(getSomeProducts(2));

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).get(0).getId()}, productMapperMock)).thenReturn(products);
        Product newProduct1 = productDAO.selectProductByID(1);
        assertEquals(products.get(0).get(0), newProduct1);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).get(1).getId()}, productMapperMock)).thenReturn(products);
        Product newProduct2 = productDAO.selectProductByID(2);
        assertEquals(products.get(0).get(1), newProduct2);

        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).get(0).getId()}, productMapperMock);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_ID,new Object[]{products.get(0).get(1).getId()}, productMapperMock);
    }

    @Test
    public void testSelectProductByName() {

        List<List<Product>> products = new ArrayList<>();
        products.add(getSomeProducts(2));

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).get(0).getName()}, productMapperMock)).thenReturn(products);
        Product newProduct1 = productDAO.selectProductByName("name1");
        assertEquals(products.get(0).get(0), newProduct1);

        when(jdbcTemplateMock.query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).get(1).getName()}, productMapperMock)).thenReturn(products);
        Product newProduct2 = productDAO.selectProductByName("name2");
        assertEquals(products.get(0).get(1), newProduct2);

        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).get(0).getName()}, productMapperMock);
        verify(jdbcTemplateMock, times(1)).query(productDAO.SQL_SELECT_BY_NAME, new Object[]{products.get(0).get(1).getName()}, productMapperMock);
    }
}
