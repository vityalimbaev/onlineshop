package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.BasketDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.BasketMapper;
import net.thumbtack.onlineshop.jdbc.model.Basket;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BasketDaoUnitTest extends BaseUnitTest {
    @InjectMocks
    private BasketDAO basketDAO;

    @Mock
    DataSource dataSourceMock;
    @Mock
    private JdbcTemplate jdbcTemplateMock;

    @Mock
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateMock;

    @Mock
    private BasketMapper basketMapperMock;

    @Test
    public void testInsertBasket(){
        Product product = getProduct(1);
        Client client = getClient(1,1);
        basketDAO.insertBasket(product, client,2);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class));
    }

    @Test
    public void testUpdateBasket() {
        Product product = getProduct(1);
        Client client = getClient(1,1);
        int count = 2;

        basketDAO.updateBasket(product,client,count);

        verify(jdbcTemplateMock, times(1))
                .update(basketDAO.SQL_UPDATE_POSITION, count, product.getId(), client.getId());
    }

    @Test
    public void testDeleteBasket(){
        Product product = getProduct(1);
        Client client = getClient(1,1);

        basketDAO.deleteBasket(product.getId(), client);

        verify(jdbcTemplateMock, times(1))
                .update(basketDAO.SQL_DELETE_POSITION, product.getId(), client.getId());
    }

    @Test
    public void testSelectBasket(){
        Product product = getProduct(1);
        Client client = getClient(1,1);
        Basket basket = new Basket();
        basket.setClientId(client.getId());
        basket.addProduct(product, 2);
        List<Basket> listBasket = new ArrayList<>();
        listBasket.add(basket);

        when(jdbcTemplateMock.query(basketDAO.SQL_SELECT_BASKET, new Object[]{client.getId()}, basketMapperMock))
                .thenReturn(listBasket);

        basketDAO.selectBasket(client);

        verify(jdbcTemplateMock, times(1))
                .query(basketDAO.SQL_SELECT_BASKET, new Object[]{client.getId()},basketMapperMock);
    }


}
