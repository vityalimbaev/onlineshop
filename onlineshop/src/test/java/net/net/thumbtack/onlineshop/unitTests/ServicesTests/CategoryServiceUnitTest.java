package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import com.sun.security.ntlm.Server;
import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import net.thumbtack.onlineshop.services.CategoryService;
import net.thumbtack.onlineshop.services.SessionService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CategoryServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private CategoryService categoryServiceMock;
    @Mock
    private CategoryDAO categoryDAOMock;
    @Mock
    private SessionService sessionServiceMock;
    @Mock
    private  CategoryService categoryServiceMock2;
    @Mock
    private User user;

    @Test
    public void testSaveCategory () throws ServerException {
        Category category = getCategory(1,0);
        CategoryDto categoryDto = createCategoryDto(category);

        User user = new User();
        user.setStatus(UserStatus.ADMIN);

        when(categoryDAOMock.insertCategory(Mockito.any(Category.class))).thenReturn(category);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);
        when(categoryDAOMock.selectCategoryById(Mockito.anyInt())).thenReturn(new Category());

        CategoryDto newCategoryDto = categoryServiceMock.saveCategory(categoryDto, "token");
        assertEquals(categoryDto, newCategoryDto);

        categoryDto.setParentId(2);
        category.setParentId(2);
        newCategoryDto = categoryServiceMock.saveCategory(categoryDto, "token");
        assertEquals(categoryDto, newCategoryDto);

        try {
            user.setStatus(UserStatus.CLIENT);
            categoryServiceMock.saveCategory(categoryDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        verify(categoryDAOMock,times(2)).insertCategory(Mockito.any(Category.class));
        verify(sessionServiceMock, times(3)).getActiveUser(Mockito.anyString());
        verify(categoryDAOMock, times(2)).selectCategoryById(Mockito.anyInt());
    }

    @Test
    public void testUpdateCategory() throws ServerException {
        Category category = getCategory(1,0);
        CategoryDto categoryDto = createCategoryDto(category);

        User user = new User();
        user.setStatus(UserStatus.ADMIN);

        when(categoryDAOMock.updateCategory(Mockito.any(Category.class))).thenReturn(category);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);
        when(categoryDAOMock.selectCategoryById(Mockito.anyInt())).thenReturn(new Category());

        CategoryDto newCategoryDto = categoryServiceMock.updateCategory(categoryDto, "token");
        assertEquals(categoryDto, newCategoryDto);

        categoryDto.setParentId(2);
        category.setParentId(2);
        newCategoryDto = categoryServiceMock.updateCategory(categoryDto, "token");
        assertEquals(categoryDto, newCategoryDto);

        try {
            user.setStatus(UserStatus.CLIENT);
            categoryServiceMock.updateCategory(categoryDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        verify(categoryDAOMock,times(2)).updateCategory(Mockito.any(Category.class));
        verify(sessionServiceMock, times(3)).getActiveUser(Mockito.anyString());
        verify(categoryDAOMock, times(1)).selectCategoryById(Mockito.anyInt());
    }

    @Test
    public void testRemoveCategory() throws ServerException {
        User user = new User();
        user.setStatus(UserStatus.ADMIN);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);
        categoryServiceMock.removeCategory(1, "token");
        verify(categoryDAOMock, times(1)).deleteCategory(1);
        verify(sessionServiceMock, times(1)).getActiveUser("token");

        try {
            user.setStatus(UserStatus.ADMIN);
            categoryServiceMock.removeCategory(1, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }
    }

    @Test
    public void testGetAllCategories() throws ServerException, NoSuchMethodException {

        final List<Category> list = getSomeCategories(2,0);
        List<CategoryDto> listCategoryDto = new ArrayList<CategoryDto>() {{
            add(createCategoryDto(list.get(0)));
            add(createCategoryDto(list.get(1)));
        }};

        User user = new User();
        user.setStatus(UserStatus.ADMIN);
        when(categoryDAOMock.selectAllCategory()).thenReturn(list);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);

        assertEquals(listCategoryDto, categoryServiceMock.getAllCategories("token"));

    }

   private CategoryDto createCategoryDto(Category category){
       CategoryDto categoryDto = new CategoryDto();
       categoryDto.setId(category.getId());
       categoryDto.setName(category.getName());
       return  categoryDto;
   }
}
