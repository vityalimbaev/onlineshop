package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.ClientMapper;
import net.thumbtack.onlineshop.jdbc.model.Client;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ClientDaoUnitTest extends BaseUnitTest {
    @InjectMocks
    protected ClientDAO clientDAO;

    @Mock
    DataSource dataSourceMock;
    @Mock
    ClientMapper clientMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;

    @Test
    public void testInsertClientDao(){
        Client client = getClient(1,1);
        Mockito.when(namedParameterJdbcTemplateMock.update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Map<String, Object> keyMap = new HashMap<String, Object>();
                keyMap.put("",1);
                ((GeneratedKeyHolder)args[2]).getKeyList().add(keyMap);
                return 1;
            }
        });
        Client clientNew = clientDAO.insertClient(client);
        assertEquals(client,clientNew);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(),
                Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class));
    }

    @Test
    public void testUpdateClientDao(){
        Client client = getClient(1,1);
        Client clientNew = clientDAO.updateClient(client);
        assertEquals(client,clientNew);
        verify(namedParameterJdbcTemplateMock, times(1))
                .update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class));
    }

    @Test
    public void testDeleteClient() {
        Client client = getClient(1,1);

        when(jdbcTemplateMock.update(clientDAO.SQL_DELETE,1)).thenReturn(0);

        Client newClient = clientDAO.deleteClient(client);
        assertEquals(client,newClient);

        verify(jdbcTemplateMock,times(1)).update(clientDAO.SQL_DELETE,1);
    }

    @Test
    public void testSelectAllClient() {

        List<Client> list = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_ALL, clientMapperMock)).thenReturn(list);

        List<Client> newList = clientDAO.selectAllClient();
        assertEquals(list, newList);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_ALL, clientMapperMock);
    }

    @Test
    public void testSelectClient() {
        List<Client> clients = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(0).getId()}, clientMapperMock)).thenReturn(clients);
        Client newClient1 = clientDAO.selectClient(1);
        assertEquals(clients.get(0), newClient1);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(1).getId()}, clientMapperMock)).thenReturn(clients);
        Client newClient2 = clientDAO.selectClient(2);
        assertEquals(clients.get(1), newClient2);

        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(0).getId()}, clientMapperMock);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_ID,new Object[]{clients.get(1).getId()}, clientMapperMock);
    }

    @Test
    public void testSelectUserByLogin() {

        List<Client> clients = getSomeClients(2);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(0).getUser().getLogin()}, clientMapperMock)).thenReturn(clients);
        Client newClient1 = clientDAO.selectClientByLogin("login1");
        assertEquals(clients.get(0), newClient1);

        when(jdbcTemplateMock.query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(1).getUser().getLogin()}, clientMapperMock)).thenReturn(clients);
        Client newClient2 = clientDAO.selectClientByLogin("login2");
        assertEquals(clients.get(1), newClient2);

        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(0).getUser().getLogin()}, clientMapperMock);
        verify(jdbcTemplateMock, times(1)).query(clientDAO.SQL_SELECT_BY_LOGIN,new Object[]{clients.get(1).getUser().getLogin()}, clientMapperMock);
    }

    @Test
    public void testInsertDeposit(){
        Client client = getClient(1,1);
        Integer deposit = 200;
        assertEquals(deposit, clientDAO.updateDeposit(deposit, client.getUser().getId()));
        verify(jdbcTemplateMock, times(1)).update(clientDAO.SQL_DEPOSIT_UPDATE, deposit, client.getUser().getId());
    }
}
