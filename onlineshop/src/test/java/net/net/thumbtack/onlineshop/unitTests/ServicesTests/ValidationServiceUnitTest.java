package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.services.ValidationService;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;

public class ValidationServiceUnitTest extends BaseUnitTest {
    @InjectMocks
    private ValidationService validationServiceMock;

    @Test(expected = ServerException.class)
    public void testNameValidator() throws ServerException, NullPointerException {
        validationServiceMock.validateName("","","");
    }

    @Test(expected = ServerException.class)
    public void testLoginValidator() throws ServerException, NullPointerException {
        validationServiceMock.validateLogin("lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll");
    }

    @Test(expected = ServerException.class)
    public void testPasswordValidator() throws ServerException, NullPointerException {
        validationServiceMock.validatePassword("passwor");
    }

    @Test(expected = ServerException.class)
    public void testEmailValidator() throws ServerException {
        validationServiceMock.validateEmail("a@gemai");
    }

    @Test(expected = ServerException.class)
    public void testPhoneValidator() throws ServerException {
        validationServiceMock.validatePhone("9320302");
    }

    @Test(expected = ServerException.class)
    public void testAddressValidator() throws ServerException {
        validationServiceMock.validateAddress(null);
    }
}
