package net.net.thumbtack.onlineshop.unitTests.ServicesTests;

import net.net.thumbtack.onlineshop.unitTests.jdbcTest.BaseUnitTest;
import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import net.thumbtack.onlineshop.services.ProductService;
import net.thumbtack.onlineshop.services.SessionService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductServiceUnitTest extends BaseUnitTest {

    @InjectMocks
    private ProductService productServiceMock;
    @Mock
    private ProductDAO productDAOMock;
    @Mock
    private SessionService sessionServiceMock;

    @Test
    public void testAddProduct() throws ServerException {

        Product product = getProduct(1);
        Product newProduct = getProduct(1);
        ProductDto productDto = createProductDto(product);
        productDto.setId(null);
        product.setId(null);

        User user = new User();
        user.setStatus(UserStatus.ADMIN);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);
        when(productDAOMock.insertProduct(product)).thenReturn(newProduct);

        assertEquals(productDto, productServiceMock.saveProduct(productDto, "token"));

        verify(productDAOMock, times(1)).insertProduct(product);
        verify(sessionServiceMock, times(1)).getActiveUser("token");

        try {
            user.setStatus(UserStatus.CLIENT);
            productServiceMock.saveProduct(productDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        try {
            user.setStatus(UserStatus.ADMIN);
            productDto.setName(null);
            productServiceMock.saveProduct(productDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, ex.getErrorCode());
        }
    }

    @Test
    public void testUpdateProduct() throws ServerException {
        Product product = getProduct(1);
        Product newProduct = getProduct(1);
        ProductDto productDto = createProductDto(product);
        productDto.setId(1);

        User user = new User();
        user.setStatus(UserStatus.ADMIN);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);
        when(productDAOMock.updateProduct(product)).thenReturn(newProduct);

        assertEquals(productDto, productServiceMock.updateProduct(productDto, "token"));

        verify(productDAOMock, times(1)).updateProduct(product);
        verify(sessionServiceMock, times(1)).getActiveUser("token");

        try {
            user.setStatus(UserStatus.CLIENT);
            productServiceMock.saveProduct(productDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }

        try {
            user.setStatus(UserStatus.ADMIN);
            productDto.setName(null);
            productServiceMock.saveProduct(productDto, "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, ex.getErrorCode());
        }
    }

    @Test
    public void testRemoveProduct() throws ServerException {

        Product product = new Product();
        product.setId(1);

        ProductDto productDto = createProductDto(product);
        productDto.setId(1);

        User user = new User();
        user.setStatus(UserStatus.ADMIN);
        when(sessionServiceMock.getActiveUser("token")).thenReturn(user);

        productServiceMock.removeProduct(productDto.getId(), "token");

        verify(productDAOMock, times(1)).deleteProduct(product.getId());
        verify(sessionServiceMock, times(1)).getActiveUser("token");

        try{
            user.setStatus(UserStatus.CLIENT);
            productServiceMock.removeProduct(productDto.getId(), "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_ACCESS_DENIED, ex.getErrorCode());
        }
    }

    @Test
    public void testSelectProduct() throws ServerException {

        ProductDto productDto = new ProductDto();
        productDto.setId(1);
        List<Product> list = getSomeProducts(2);
        ProductDto returnedProductDto = createProductDto(list.get(0));
        returnedProductDto.setId(1);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(new User());
        when(productDAOMock.selectProductByID(productDto.getId())).thenReturn(list.get(0));

        assertEquals(returnedProductDto, productServiceMock.getProduct(productDto.getId(), "token"));

        verify(productDAOMock, times(1)).selectProductByID(1);
        verify(sessionServiceMock, times(1)).getActiveUser("token");

        try{
            productServiceMock.getProduct(productDto.getId(), "token");
        }catch (ServerException ex){
            assertEquals(ServerErrorCode.SERVER_PRODUCT_NOT_FOUND, ex.getErrorCode()) ;
        }
    }

    @Test
    public void testSelectAllProducts() throws ServerException {

        List<Product> products = getSomeProducts(4);
        List<ProductDto> productsDto = new ArrayList<>();
        productsDto.add(createProductDto(products.get(0)));
        productsDto.add(createProductDto(products.get(1)));
        productsDto.add(createProductDto(products.get(2)));
        productsDto.add(createProductDto(products.get(3)));
        productsDto.get(0).setId(1);
        productsDto.get(1).setId(2);
        productsDto.get(2).setId(3);
        productsDto.get(3).setId(4);

        when(sessionServiceMock.getActiveUser("token")).thenReturn(new User());
        when(productDAOMock.selectAllProduct()).thenReturn(products);


        assertEquals(productsDto, productServiceMock.getAllProducts(new int[0],"product","token"));

        verify(productDAOMock, times(1)).selectAllProduct();
        verify(sessionServiceMock, times(1)).getActiveUser("token");

    }

    private ProductDto createProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setName(product.getName());
        productDto.setPrice(product.getPrice());
        productDto.setCount(product.getCount());
        return productDto;
    }
}
