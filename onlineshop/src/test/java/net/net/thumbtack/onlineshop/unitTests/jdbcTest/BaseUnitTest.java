package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.dto.AdminDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.*;
import org.junit.Before;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


public class BaseUnitTest {

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    public User getUser(int userId) {
        User user = new User();
        user.setId(userId);
        user.setLogin("login" + userId);
        user.setPassword("password");
        user.setFirstName("name1");
        user.setSecondName("name2");
        user.setThirdName("name3");
        return user;
    }

    public Admin getAdmin(int userID, int adminId) {
        Admin admin = new Admin();
        User user = getUser(userID);
        user.setStatus(UserStatus.ADMIN);
        admin.setUser(user);
        admin.setPosition("position");
        admin.setId(adminId);
        admin.getUser().setStatus(UserStatus.ADMIN);
        return admin;
    }

    public Client getClient(int clientID, int userId) {
        Client client = new Client();
        User user = getUser(userId);
        user.setStatus(UserStatus.CLIENT);
        client.setUser(user);
        client.setBasket(new Basket());
        client.setPhone("123456");
        client.setEmail("email" + clientID + "@gmail.com");
        client.setAddress("address1");
        client.setDeposit(500000);
        client.setId(clientID);
        return client;
    }

    public Product getProduct(int productID) {
        Product product = new Product();
        product.setId(productID);
        product.setName("name" + productID);
        product.setCount(3);
        product.setPrice(250);
        return product;
    }

    public Category getCategory(int categoryID, int countProducts) {
        Category category = new Category();
        category.setId(categoryID);
        category.setName("name" + categoryID);

        return category;
    }

    public List<Admin> getSomeAdmins(int count) {
        List<Admin> listAdmin = new ArrayList<Admin>();
        for (int i = 1; i <= count; i++) {
            listAdmin.add(getAdmin(i, i));
        }
        return listAdmin;
    }

    public List<Client> getSomeClients(int count) {
        List<Client> listClient = new ArrayList<Client>();
        for (int i = 1; i <= count; i++) {
            listClient.add(getClient(i, i));
        }
        return listClient;
    }

    public List<Product> getSomeProducts(int count) {
        List<Product> listProduct = new ArrayList<Product>();
        for (int i = 1; i <= count; i++) {
            listProduct.add(getProduct(i));
        }
        return listProduct;
    }

    public List<Category> getSomeCategories(int count, int countProducts) {
        List<Category> listCategories = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            listCategories.add(getCategory(i, countProducts));
        }
        return listCategories;
    }

    public List<User> getSomeUsers(int count) {
        List<User> listUser = new ArrayList<User>();
        for (int i = 1; i <= count; i++) {
            listUser.add(getUser(i));
        }
        return listUser;
    }

    public UserDto getUserDto(Admin admin) {
        UserDto userDto = new UserDto();
        userDto.setId(admin.getUser().getId());
        userDto.setFirstName(admin.getUser().getFirstName());
        userDto.setSecondName(admin.getUser().getSecondName());
        userDto.setThirdName(admin.getUser().getThirdName());
        userDto.setPosition(admin.getPosition());
        userDto.setLogin(admin.getUser().getLogin());
        userDto.setPassword(admin.getUser().getPassword());
        userDto.setStatus(UserStatus.ADMIN);
        return userDto;
    }

    public UserDto getUserDto(Client client) {
        UserDto userDto = new UserDto();
        userDto.setId(client.getUser().getId());
        userDto.setFirstName(client.getUser().getFirstName());
        userDto.setSecondName(client.getUser().getSecondName());
        userDto.setThirdName(client.getUser().getThirdName());
        userDto.setLogin(client.getUser().getLogin());
        userDto.setPassword(client.getUser().getPassword());
        userDto.setStatus(UserStatus.CLIENT);
        userDto.setDeposit(client.getDeposit());
        userDto.setAddress(client.getAddress());
        userDto.setPhone(client.getPhone());
        userDto.setEmail(client.getEmail());
        return userDto;
    }

    public Purchase getPurchase(Product product, int clientId, int count){
        Purchase purchase = new Purchase();
        purchase.setId(product.getId());
        purchase.setProductId(product.getId());
        purchase.setClientId(clientId);
        purchase.setPrice(product.getPrice());
        purchase.setCount(count);
        purchase.setName(product.getName());
        return purchase;
    }

    public ProductDto toProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setCount(product.getCount());
        productDto.setPrice(product.getPrice());

        if (product.getCategories() != null) {
            List<String> listNames = new ArrayList<>();
            for (Category category : product.getCategories()) {
                listNames.add(category.getName());
            }
            productDto.setCategories(listNames);
        }

        return productDto;
    }


}
