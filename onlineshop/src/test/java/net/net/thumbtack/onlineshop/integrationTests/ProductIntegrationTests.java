package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.junit.Test;
import org.springframework.http.*;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ProductIntegrationTests extends BaseIntegrationTest {

    @Test
    public void testSaveProduct() {
        saveAdmin();
        ProductDto productDto = toProductDto(getProduct(1));
        ProductDto response = restTemplate.postForObject(rootUrl + "products", productDto, ProductDto.class);
        assertEquals(productDto, response);
    }

    @Test
    public void testUpdateProduct() {
        saveAdmin();
        ProductDto productDto = saveProduct(toProductDto(getProduct(1)));
        productDto.setCategoriesId(new ArrayList<Integer>() {{
            add(saveCategory(1).getId());
        }});

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.set("Cookie", StatefulRestTemplateInterceptor.getCookie());
        HttpEntity<ProductDto> entity = new HttpEntity<>(productDto,headers);
        ResponseEntity<ProductDto>responseEntity = restTemplate.exchange(rootUrl + "products/1", HttpMethod.PUT,entity,ProductDto.class);
        restTemplate.put(rootUrl + "products/1", productDto);
        assertEquals(productDto, responseEntity.getBody());
    }

    @Test()
    public void testDeleteProduct() {
        saveAdmin();
        saveProduct(toProductDto(getProduct(1)));
        restTemplate.delete(rootUrl + "products/1");
    }

    @Test
    public void testSelectProduct() {
        saveAdmin();
        ProductDto productDto = saveProduct(toProductDto(getProduct(1)));
        ProductDto response = restTemplate.getForObject(rootUrl+"products/1", ProductDto.class);
        productDto.setCategoriesId(null);
        assertEquals(productDto, response);
    }

    @Test
    public void testSelectProducts() {
        saveAdmin();
        ProductDto productDto1 = saveProduct(toProductDto(getProduct(1)));
        ProductDto productDto2 = saveProduct(toProductDto(getProduct(2)));
        productDto1.setCategoriesId(null);
        productDto2.setCategoriesId(null);

        List<ProductDto> list = new ArrayList<>();
        list.add(productDto1);
        list.add(productDto2);

        ResponseEntity<ProductDto[]> response = restTemplate.getForEntity(rootUrl+"products", ProductDto[].class);

        List<ProductDto> listResponse = Arrays.asList(response.getBody());
        assertEquals(list, listResponse);
    }


    private CategoryDto saveCategory(int id) {
        CategoryDto categoryDto = toCategoryDto(getCategory(id));
        return restTemplate.postForObject(rootUrl + "categories", categoryDto, CategoryDto.class);
    }

    private ProductDto saveProduct(ProductDto productDto) {
        ResponseEntity<ProductDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "products", productDto, ProductDto.class);
        return responseEntity.getBody();

    }

    private UserDto saveAdmin() {
        UserDto userDto = getUserDto(getAdmin(1, 1));
        ResponseEntity<UserDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);
        return userDto;
    }



}
