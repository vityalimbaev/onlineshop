package net.net.thumbtack.onlineshop.unitTests.jdbcTest;

import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.daoMappers.CategoryMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import javax.sql.DataSource;
import java.beans.Transient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryDaoUnitTest extends BaseUnitTest {

    @InjectMocks
    CategoryDAO categoryDAO;
    @Mock
    DataSource dataSourceMock;
    @Mock
    CategoryMapper categoryMapperMock;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplateMock ;
    @Mock
    JdbcTemplate jdbcTemplateMock;
    @Mock
    SqlParameterSource sqlParameterSourceMock;

    @Test
    public  void  testInsertCategory(){
        Category category = getCategory(1,1);
        Mockito.when(namedParameterJdbcTemplateMock.update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class))).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Map<String, Object> keyMap = new HashMap<String, Object>();
                keyMap.put("",1);
                ((GeneratedKeyHolder)args[2]).getKeyList().add(keyMap);
                return 1;
            }
        });

        Category categoryNew = categoryDAO.insertCategory(category);
        assertEquals(category, categoryNew);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class), Mockito.any(GeneratedKeyHolder.class));
    }

    @Test
    public void testUpdateCategory() {

        Category category = getCategory(1,1);
        Category categoryNew = categoryDAO.updateCategory(category);
        assertEquals(category, categoryNew);
        verify(namedParameterJdbcTemplateMock, times(1)).update(Mockito.anyString(), Mockito.any(MapSqlParameterSource.class));
    }

    @Test
    public void testSelectAllCategory() {
        List<List<Category>> categories = new ArrayList<List<Category>>(){{
            add(getSomeCategories(1,1));
        }};
        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_ALL, categoryMapperMock)).thenReturn(categories);

        List<Category> listNew = categoryDAO.selectAllCategory();
        assertEquals(categories.get(0),listNew);
        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_ALL, categoryMapperMock);
    }

    @Test
    public void selectCategoryById() {
        List<List<Category>> categories = new ArrayList<List<Category>>(){{
            add(getSomeCategories(1,1));
        }};
        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_BY_ID, new Object[]{categories.get(0).get(0).getId()}, categoryMapperMock)).thenReturn(categories);

        Category categoryNew = categoryDAO.selectCategoryById(categories.get(0).get(0).getId());
        assertEquals(categories.get(0).get(0),categoryNew);
        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_BY_ID,new Object[]{categories.get(0).get(0).getId()}, categoryMapperMock);
    }

    @Test
    public void testSelectCategoryByName() {
       List<List<Category>> categories = new ArrayList<List<Category>>(){{
           add(getSomeCategories(1,1));
       }};

        when(jdbcTemplateMock.query(categoryDAO.SQL_SELECT_BY_NAME, new Object[]{categories.get(0).get(0).getName()}, categoryMapperMock)).thenReturn(categories);

        Category categoryNew = categoryDAO.selectCategoryByName(categories.get(0).get(0).getName());
        assertEquals(categories.get(0).get(0),categoryNew);

        verify(jdbcTemplateMock, times(1) ).query(categoryDAO.SQL_SELECT_BY_NAME,
                new Object[]{categories.get(0).get(0).getName()}, categoryMapperMock);
    }
}
