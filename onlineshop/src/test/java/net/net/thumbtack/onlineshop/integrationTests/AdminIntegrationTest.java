package net.net.thumbtack.onlineshop.integrationTests;

import net.thumbtack.onlineshop.dto.UserDto;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
public class AdminIntegrationTest extends BaseIntegrationTest {

    @Test
    public void testRegistrationAdmin() {
        UserDto userDto = getUserDto( getAdmin(1,1));
        ResponseEntity<UserDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "admins", userDto, UserDto.class);

        userDto.setId(1);
        userDto.setPassword(null);
        assertEquals(userDto,responseEntity.getBody());
    }

    @Test
    public void testUpdateAdmin() {
        UserDto userDto = saveAdmin();
        userDto.setNewPassword("password");
        userDto.setOldPassword("password");
        userDto.setFirstName("НовоеИмя");
        userDto.setPosition("newPos");

        restTemplate.put(rootUrl+"admins", userDto);

        userDto.setOldPassword(null);
        userDto.setNewPassword(null);
        userDto.setPassword(null);

        UserDto response = restTemplate.getForObject(rootUrl+"admins", UserDto.class);

        assertEquals(userDto, response) ;
    }

    private UserDto saveAdmin(){
        UserDto userDto = getUserDto( getAdmin(1,1));
        ResponseEntity<UserDto> responseEntity = restTemplate
                .postForEntity(rootUrl + "admins", userDto, UserDto.class);
        userDto.setId(1);
        return userDto;
    }



}
