package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

    private static Integer maxNameLength;
    private static Integer minPasswordLength;
    private static final Integer defaultMinPasswordLength = 8;
    private static final Integer defaultMaxNameLength = 50;

    @Autowired
    public ValidationService() {
    }


    public void validateName(String firstName, String secondName, String thirdName) throws ServerException {
        if (firstName == null) throw new ServerException(ServerErrorCode.SERVER_NULL_NAME, "firstName");
        if (secondName == null) throw new ServerException(ServerErrorCode.SERVER_NULL_NAME, "secondName");

        String regex = "[а-яёА-ЯЁ-]{1,1000}";
        if (!firstName.matches(regex) || firstName.length() > getMaxNameLength())
            throw new ServerException(ServerErrorCode.SERVER_INVALID_NAME, "firstName");

        if (!secondName.matches(regex) || secondName.length() > getMaxNameLength())
            throw new ServerException(ServerErrorCode.SERVER_INVALID_NAME, "secondName");

        if (thirdName != null) {
            if (!thirdName.matches(regex) || thirdName.length() > getMaxNameLength())
                throw new ServerException(ServerErrorCode.SERVER_INVALID_NAME, "thirdName");
        }
    }

    public void validateLogin(String login) throws ServerException {
        if (login == null)
            throw new ServerException(ServerErrorCode.SERVER_NULL_LOGIN, "login");
        if (!login.matches("([a-яёА-ЯЁA-Za-z0-9]){1,1000}") || login.length() > getMaxNameLength())
            throw new ServerException(ServerErrorCode.SERVER_INVALID_LOGIN, "login");
    }

    public void validatePassword(String password) throws ServerException {
        if (password == null)
            throw new ServerException(ServerErrorCode.SERVER_NULL_PASSWORD, "password");
        if (!password.matches("([A-Za-z0-9_]){1,1000}") || password.length() < getMinPasswordLength())
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PASSWORD, "password");
    }

    public void validateEmail(String email) throws ServerException {
        if (!email.matches("^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_EMAIL, "email");
    }

    public void validatePhone(String phone) throws ServerException {
        if (!phone.matches("^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{11,12}$"))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PHONE, "phone");
    }

    public void validateAddress(String address) throws ServerException {
        if (address == null) throw new ServerException(ServerErrorCode.SERVER_INVALID_ADDRESS, "address");
    }

    public static Integer getMaxNameLength() {
        return (maxNameLength == null ? defaultMaxNameLength : maxNameLength);
    }

    public static Integer getMinPasswordLength() {
        return (minPasswordLength == null ? defaultMinPasswordLength : minPasswordLength);
    }

    public static void setMinPasswordLength(Integer min_password_length) {
        ValidationService.minPasswordLength = min_password_length;
    }

    public static void setMaxNameLength(Integer max_name_length) {
        ValidationService.maxNameLength = max_name_length;
    }


}
