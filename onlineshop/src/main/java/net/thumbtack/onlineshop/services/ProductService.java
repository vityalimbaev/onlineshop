package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductService {

    private CategoryDAO categoryDAO;
    private ProductDAO productDAO;
    private SessionService sessionService;

    @Autowired
    public ProductService(CategoryDAO categoryDAO, ProductDAO productDAO, SessionService sessionService) {
        this.categoryDAO = categoryDAO;
        this.productDAO = productDAO;
        this.sessionService = sessionService;
    }

    public ProductDto saveProduct(ProductDto productDto, String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        checkProductDto(productDto);
        Product product = fromProductDto(productDto);
        productDAO.insertProduct(product);
        if (productDto.getCategoriesId() != null) {
            productDAO.insertProductCategories(productDto.getCategoriesId(), product.getId());
        }
        return productDto;
    }

    public ProductDto updateProduct(ProductDto productDto, String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        checkProductDto(productDto);
        Product product = fromProductDto(productDto);
        productDAO.updateProduct(product);
        productDAO.deleteProductCategory(product.getId());
        if (productDto.getCategoriesId() != null) {
            productDAO.insertProductCategories(productDto.getCategoriesId(), product.getId());
        }
        return productDto;
    }

    public void removeProduct(int id, String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        productDAO.deleteProduct(id);
    }

    public ProductDto getProduct(int id, String token) throws ServerException {
        sessionService.getActiveUser(token);
        Product product = productDAO.selectProductByID(id);
        if (product == null)
            throw new ServerException(ServerErrorCode.SERVER_PRODUCT_NOT_FOUND, "id=" + id);
        return toProductDto(product);
    }

    public List<ProductDto> getAllProducts(int[] categories, String order, String token) throws ServerException {
        sessionService.getActiveUser(token);
        List<Product> products = productDAO.selectAllProduct();
        List<Product> chosenProducts = new ArrayList<>();

        if (categories == null) {
            chosenProducts.addAll(products);
            return sortProductsByProduct(chosenProducts);
        } else if (categories.length == 0) {
            for (Product product : products) {
                if (product.getCategories() == null || product.getCategories().size() == 0) {
                    chosenProducts.add(product);
                }
            }
            return sortProductsByProduct(chosenProducts);
        } else {
            List<Category> listCategory = new ArrayList<>();
            for (int id : categories) {
                listCategory.add(categoryDAO.selectCategoryById(id));
            }

            for (Product product : products) {
                if (product.getCategories() != null && product.getCategories().containsAll(listCategory)) {
                    chosenProducts.add(product);
                }
            }
        }

        if (order.equals("product")) {
            return sortProductsByProduct(chosenProducts);
        } else if (order.equals("category")) {
            return sortProductsByCategory(chosenProducts);
        }
        return toListProductDto(products);
    }

    private List<ProductDto> sortProductsByProduct(List<Product> products) throws ServerException {
        List<ProductDto> response = new ArrayList<>();
        for (Product product : products) {
            response.add(toProductDto(product));
        }
        Collections.sort(response, new Comparator<ProductDto>() {
            public int compare(ProductDto one, ProductDto other) {
                return one.getName().compareToIgnoreCase(other.getName());
            }
        });

        return response;
    }

    private List<ProductDto> sortProductsByCategory(List<Product> products) throws ServerException {
        List<Integer> categories = productDAO.selectAllProductCategory();
        List<ProductDto> response = new ArrayList<>();
        for (Product product : products) {
            for (final Category category : product.getCategories()) {
                if (!categories.contains(category.getId())) continue;
                ProductDto productDto = toProductDto(product);
                productDto.setCategories(new ArrayList<String>() {{
                    add(category.getName());
                }});
                response.add(productDto);
            }
        }

        Collections.sort(response, new Comparator<ProductDto>() {
            public int compare(ProductDto one, ProductDto other) {
                return one.getCategories().get(0).compareToIgnoreCase(other.getCategories().get(0));
            }
        });

        return response;
    }

    private Product fromProductDto(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setCount(productDto.getCount());
        return product;
    }

    private ProductDto toProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setCount(product.getCount());
        productDto.setPrice(product.getPrice());

        if (product.getCategories() != null) {
            List<String> listNames = new ArrayList<>();
            for (Category category : product.getCategories()) {
                listNames.add(category.getName());
            }
            productDto.setCategories(listNames);
        }

        return productDto;
    }

    private List<ProductDto> toListProductDto(List<Product> products) {
        List<ProductDto> listProductsDto = new ArrayList<>();
        for (Product product : products) {
            listProductsDto.add(toProductDto(product));
        }
        return listProductsDto;
    }

    public void checkProductDto(ProductDto productDto) throws ServerException {
        if (productDto.getName() == null)
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "name");
        if (productDto.getPrice() == null || productDto.getPrice() < 1)
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "price");
        if (productDto.getCategories() != null) {
            for (Integer id : productDto.getCategoriesId()) {
                if (categoryDAO.selectCategoryById(id) == null) {
                    throw new ServerException(ServerErrorCode.SERVER_CATEGORY_NOT_FOUND, "categoryId=" + id);
                }
            }
        }
    }
}
