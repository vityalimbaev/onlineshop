package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {
    private ClientDAO clientDAO;
    private SessionService sessionService;
    private UserService userService;
    private ValidationService validationService;

    @Autowired
    public ClientService(ClientDAO clientDAO, SessionService sessionService, UserService userService, ValidationService validationService) {
        this.clientDAO = clientDAO;
        this.sessionService = sessionService;
        this.userService = userService;
        this.validationService = validationService;
    }

    public UserDto saveClient(UserDto userDto) throws ServerException {
        userDto.setPhone(preparedPhone(userDto.getPhone()));
        checkUserDtoRequest(userDto);
        userDto.setStatus(UserStatus.CLIENT);
        User user = userService.saveUser(userDto);

        Client client = fromUserDto(userDto);
        client.setUser(user);
        client = clientDAO.insertClient(client);
        sessionService.setActiveUser(user);
        userDto = toUserDto(client);
        return userDto;
    }

    public UserDto updateClient(UserDto userDto, String token) throws ServerException {
        userDto.setPhone(preparedPhone(userDto.getPhone()));
        checkUserDtoRequest(userDto);
        User user = sessionService.getActiveUser(token);
        if (!userDto.getOldPassword().equals(user.getPassword()))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PASSWORD, "oldPassword");

        Client client = fromUserDto(userDto);
        client.setId(userDto.getId());
        userService.updateUser(user, userDto);
        clientDAO.updateClient(client);
        client.setUser(user);
        userDto = toUserDto(client);
        userDto.setDeposit(null);
        return userDto;
    }

    public UserDto putCash(final Integer cash, final String token) throws ServerException {
        if (cash < 1) throw new ServerException(ServerErrorCode.SERVER_INCORRECT_DEPOSIT, "deposit");

        UserDto userDto = sessionService.getProfileByToken(token);

        Client client = fromUserDto(userDto);
        Integer deposit = client.addCash(cash);
        client.setDeposit(clientDAO.updateDeposit(deposit, client.getId()));
        userDto.setDeposit(deposit);
        userDto.setPassword(null);
        return userDto;
    }

    public List<UserDto> getAllClient(String token) throws ServerException {
        sessionService.getActiveUser(token);
        List<UserDto> clients = new ArrayList<>();
        for (Client client : clientDAO.selectAllClient()) {
            UserDto userDto = toUserDto(client);
            userDto.setStatus(client.getUser().getStatus());
            clients.add(userDto);
        }
        return clients;
    }

    public Client fromUserDto(UserDto userDto) {
        Client client = new Client();
        client.setId(userDto.getId());
        client.setDeposit(userDto.getDeposit());
        client.setEmail(userDto.getEmail());
        client.setPhone(userDto.getPhone());
        client.setAddress(userDto.getAddress());
        return client;
    }

    public UserDto toUserDto(Client client) {
        UserDto userDto = new UserDto();
        userDto.setId(client.getId());
        userDto.setFirstName(client.getUser().getFirstName());
        userDto.setSecondName(client.getUser().getSecondName());
        userDto.setThirdName(client.getUser().getThirdName());
        userDto.setLogin(client.getUser().getLogin());
        userDto.setPhone(client.getPhone());
        userDto.setEmail(client.getEmail());
        userDto.setAddress(client.getAddress());
        userDto.setDeposit(client.getDeposit());
        return userDto;
    }

    private void checkUserDtoRequest(UserDto userDto) throws ServerException {
        validationService.validateEmail(userDto.getEmail());
        validationService.validatePhone(userDto.getPhone());
        validationService.validateAddress(userDto.getAddress());
    }

    private String preparedPhone(String phone) {
        if (phone != null) {
            phone = phone.replaceAll("\\+7", "8");
            phone = phone.replaceAll(" ", "");
            phone = phone.replaceAll("[\\\\()]", "");
            phone = phone.replaceAll("[\\\\-]", "");
        }
        return phone;
    }

}
