package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.PurchaseBasketDto;
import net.thumbtack.onlineshop.jdbc.dao.BasketDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.dao.PurchaseDAO;
import net.thumbtack.onlineshop.jdbc.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PurchaseService {

    private ClientDAO clientDAO;
    private ProductDAO productDAO;
    private PurchaseDAO purchaseDAO;
    private BasketDAO basketDAO;
    private SessionService sessionService;

    @Autowired
    public PurchaseService(ClientDAO clientDAO, ProductDAO productDAO, PurchaseDAO purchaseDAO, BasketDAO basketDAO, SessionService sessionService) {
        this.clientDAO = clientDAO;
        this.productDAO = productDAO;
        this.purchaseDAO = purchaseDAO;
        this.basketDAO = basketDAO;
        this.sessionService = sessionService;
    }

    public ProductDto purchaseProduct(final ProductDto productDto, String token) throws ServerException {
        User user = sessionService.getActiveUser(token);
        if (user == null) throw new ServerException(ServerErrorCode.SERVER_USER_NOT_FOUND, "login");

        StatusCheckUtility.checkClientStatus(user);
        Client client = clientDAO.selectClientByLogin(user.getLogin());

        if (productDto.getId() == null) throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "id");
        Product product = productDAO.selectProductByID(productDto.getId());
        if (product == null) throw new ServerException(ServerErrorCode.SERVER_PRODUCT_NOT_FOUND, "id");
        if (productDto.getCount() == null) productDto.setCount(1);

        checkRequest(productDto, product);

        if (productDto.getCount() * product.getPrice() > client.getDeposit())
            throw new ServerException(ServerErrorCode.SERVER_CLIENT_DEPOSIT, "deposit");

        List<ProductDto> list = new ArrayList<>();
        list.add(productDto);
        buyBasket(list, client);
        return productDto;
    }

    public PurchaseBasketDto purchaseBasket(List<ProductDto> orderList, String token) throws ServerException, Exception {
        User user = sessionService.getActiveUser(token);
        Client client = clientDAO.selectClientByLogin(user.getLogin());
        Basket basket = basketDAO.selectBasket(client);
        PurchaseBasketDto purchaseBasketDto = new PurchaseBasketDto();
        int sum = 0;

        for (ProductDto productDto : orderList) {
            Product position = basket.getProductByID(productDto.getId());
            Product product = productDAO.selectProductByID(productDto.getId());
            if (productDto.getCount() == null) productDto.setCount(position.getCount());

            try {
                checkRequest(productDto, product);
            }catch (ServerException ex){
                continue;
            }


            if (productDto.getCount() >= position.getCount()) {
                productDto.setCount(position.getCount());
                basket.removeProduct(position);
                basketDAO.deleteBasket(position.getId(), client);
            } else {
                position.setCount(position.getCount() - productDto.getCount());
            }
            basketDAO.updateBasket(position, client, position.getCount());
            sum += productDto.getPrice();
            purchaseBasketDto.addToBougth(productDto);
            purchaseBasketDto.addToRemaining(toProductDto(position));
        }

        if (sum > client.getDeposit())
            throw new ServerException(ServerErrorCode.SERVER_CLIENT_DEPOSIT, "deposit");
        if(purchaseBasketDto != null)
            buyBasket(purchaseBasketDto.getBought(), client);
        return purchaseBasketDto;
    }

    private void buyBasket(List<ProductDto> orderList, Client client) {
        for (ProductDto productDto : orderList) {
            Product product = productDAO.selectProductByID(productDto.getId());

            Purchase purchase = new Purchase();
            purchase.setProductId(product.getId());
            purchase.setName(product.getName());
            purchase.setPrice(product.getPrice());
            purchase.setCount(productDto.getCount());
            purchase.setClientId(client.getId());

            Integer deposit = client.getDeposit() - (purchase.getCount() * purchase.getPrice());
            product.setCount(product.getCount() - purchase.getCount());

            clientDAO.updateDeposit(deposit, client.getId());
            purchaseDAO.insertPurchase(purchase);
            productDAO.updateProduct(product);
        }
    }

    public void checkRequest(ProductDto productDto, Product product) throws ServerException {
        if (!productDto.getName().equals(product.getName()))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "name");
        if (!productDto.getPrice().equals(product.getPrice()))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "price");
        if (productDto.getCount() > product.getCount())
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PRODUCT_DATA, "count");
    }

    public ProductDto toProductDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setCount(product.getCount());
        productDto.setName(product.getName());
        productDto.setId(product.getId());
        productDto.setPrice(product.getPrice());
        return productDto;
    }


}
