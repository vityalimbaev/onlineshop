package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {
    private UserDAO userDAO;
    private ValidationService validationService;

    @Autowired
    public UserService(UserDAO userDAO, ValidationService validationService) {
        this.validationService = validationService;
        this.userDAO = userDAO;
    }

    public User saveUser(UserDto dto) throws ServerException {
        validationService.validateName(dto.getFirstName(), dto.getSecondName(), dto.getThirdName());
        validationService.validateLogin(dto.getLogin());
        validationService.validatePassword(dto.getPassword());

        if (userDAO.selectUserByLogin(dto.getLogin()) != null) {
            throw new ServerException(ServerErrorCode.SERVER_LOGIN_ALREADY_EXIST, "login");
        }

        User user = new User();
        user.setLogin(dto.getLogin());
        user.setFirstName(dto.getFirstName());
        user.setSecondName(dto.getSecondName());
        user.setThirdName(dto.getThirdName());
        user.setPassword(dto.getPassword());
        user.setStatus(dto.getStatus());

        return userDAO.insertUser(user);
    }

    public User updateUser(User user, UserDto userDto) throws ServerException {
        validationService.validateName(user.getFirstName(), user.getSecondName(), user.getThirdName());
        validationService.validatePassword(user.getPassword());

        user.setPassword(userDto.getNewPassword());
        user.setFirstName(userDto.getFirstName());
        user.setSecondName(userDto.getSecondName());
        user.setThirdName(userDto.getThirdName());

        return userDAO.updateUser(user);
    }

}
