package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;

public class StatusCheckUtility {
    public static void checkAdminStatus(User user) throws ServerException {
        if (user.getStatus() != UserStatus.ADMIN)
            throw new ServerException(ServerErrorCode.SERVER_ACCESS_DENIED, "userStatus");
    }

    public static void checkClientStatus(User user) throws ServerException {
        if (user.getStatus() != UserStatus.CLIENT)
            throw new ServerException(ServerErrorCode.SERVER_ACCESS_DENIED, "userStatus");
    }
}
