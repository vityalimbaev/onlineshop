package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.AnalyticsDto;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.dao.PurchaseDAO;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.Purchase;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class PurchaseAnalytics {
    private SessionService sessionService;
    private PurchaseDAO purchaseDAO;
    private ProductDAO productDAO;
    private CategoryDAO categoryDAO;

    @Autowired
    public PurchaseAnalytics(SessionService sessionService, PurchaseDAO purchaseDAO, ProductDAO productDAO, CategoryDAO categoryDAO) {
        this.sessionService = sessionService;
        this.purchaseDAO = purchaseDAO;
        this.productDAO = productDAO;
        this.categoryDAO = categoryDAO;
    }

    public AnalyticsDto getPurchaseAnalytics(
            int[] clients, String[] names, int[] categories, String sort, String direction, int offset, int limit, String token) throws ServerException {

        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkAdminStatus(user);
        List<Purchase> purchases;

        if (categories == null) purchases = purchaseDAO.selectAllPurchases();
        else purchases = getPurchasesByCategories(categories);

        if (clients != null && clients.length != 0) {
            List<Purchase> purchasesByClient = getPurchasesByClients(clients);
            List<Purchase> removingList = new ArrayList<>();
            for (Purchase purchase : purchases) {
                if (!purchasesByClient.contains(purchase)) {
                    removingList.add(purchase);
                }
            }
            purchases.removeAll(removingList);
        }

        if (names != null && names.length != 0) {
            List<Purchase> purchasesByClient = getPurchasesByNames(names);
            List<Purchase> removingList = new ArrayList<>();
            for (Purchase purchase : purchases) {
                if (!purchasesByClient.contains(purchase)) {
                    removingList.add(purchase);
                }
            }
            purchases.removeAll(removingList);
        }

        purchases = sortPurchases(sort, direction, purchases);
        purchases = purchases.subList(
                offset < purchases.size() ? offset : 0,
                Math.min(offset + (limit==0?purchases.size():limit), purchases.size())
        );

        int[] sumAndCount = getSumAndCount(purchases);
        return toAnalyticsDto(purchases, sumAndCount[0], sumAndCount[1]);
    }

    private List<Purchase> getPurchasesByClients(int[] clients) {
        List<Purchase> purchases = new ArrayList<>();
        for (int id : clients) {
            List<Purchase> list = purchaseDAO.selectPurchasesByClient(id);
            if (list != null) {
                purchases.addAll(list);
            }
        }
        return purchases;
    }

    private List<Purchase> getPurchasesByNames(String[] names) {
        List<Purchase> purchases = new ArrayList<>();
        for (String name : names) {
            List<Purchase> list = purchaseDAO.selectPurchasesByName(name);
            if (list != null) {
                purchases.addAll(list);
            }
        }
        return purchases;
    }

    private List<Purchase> getPurchasesByCategories(int[] categories) throws ServerException {
        List<Purchase> purchases = new ArrayList<>();
        for (int id : categories) {
            if(categoryDAO.selectCategoryById(id) == null) throw new ServerException(ServerErrorCode.SERVER_CATEGORY_NOT_FOUND, "id=2");
            List<Product> products = productDAO.selectAllProductByCategory(id);
            if (products != null) {
                for (Product product : products) {
                    List<Purchase> list = purchaseDAO.selectPurchasesByName(product.getName());
                    if (list != null) {
                        purchases.addAll(list);
                    }
                }
            }
        }
        return purchases;
    }

    private List<Purchase> sortPurchases(String sort, String direction, List<Purchase> purchases) {
        switch (sort) {
            case "name":
                Collections.sort(purchases, new Comparator<Purchase>() {
                    @Override
                    public int compare(Purchase o1, Purchase o2) {
                        return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });
                break;
            case "price":
                Collections.sort(purchases, new Comparator<Purchase>() {
                    @Override
                    public int compare(Purchase o1, Purchase o2) {
                        return o1.getPrice().compareTo(o2.getPrice());
                    }
                });
                break;
            case "count":
                Collections.sort(purchases, new Comparator<Purchase>() {
                    @Override
                    public int compare(Purchase o1, Purchase o2) {
                        return o1.getCount().compareTo(o2.getCount());
                    }
                });
                break;
        }
        if (direction.equals("down")) Collections.reverse(purchases);
        return purchases;
    }

    private int[] getSumAndCount(final List<Purchase> purchases) {
        int[] sumAndCount = new int[2];
        sumAndCount[0] = 0;
        sumAndCount[1] = 0;
        for (Purchase purchase : purchases) {
            sumAndCount[0] += purchase.getPrice();
            sumAndCount[1] += purchase.getCount();
        }
        return sumAndCount;
    }

    private AnalyticsDto toAnalyticsDto(List<Purchase> purchases, int sum, int count) {
        AnalyticsDto analyticsDto = new AnalyticsDto();
        analyticsDto.setPurchases(purchases);
        analyticsDto.setCount(count);
        analyticsDto.setGain(sum);
        return analyticsDto;
    }
}
