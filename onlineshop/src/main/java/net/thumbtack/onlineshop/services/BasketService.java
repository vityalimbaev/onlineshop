package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.BasketDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.jdbc.dao.BasketDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.ProductDAO;
import net.thumbtack.onlineshop.jdbc.model.Basket;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class BasketService {

    private ProductDAO productDAO;
    private ClientDAO clientDAO;
    private BasketDAO basketDAO;
    private SessionService sessionService;

    @Autowired
    public BasketService(ProductDAO productDAO, ClientDAO clientDAO, BasketDAO basketDAO, SessionService sessionService) {
        this.productDAO = productDAO;
        this.clientDAO = clientDAO;
        this.basketDAO = basketDAO;
        this.sessionService = sessionService;
    }

    public BasketDto savePosition(ProductDto productDto, String token) throws ServerException {
        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkClientStatus(user);
        Product product = productDAO.selectProductByID(productDto.getId());
        Client client = clientDAO.selectClientByLogin(user.getLogin());
        checkProductAndProductDto(product, productDto);

        basketDAO.insertBasket(product, client, productDto.getCount());
        Basket basket = new Basket();
        basket.addProduct(product, productDto.getCount() == null ? 1 : productDto.getCount());
        return toBasketDto(basket);
    }

    public BasketDto updatePosition(ProductDto productDto, String token) throws ServerException {
        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkClientStatus(user);
        Client client = clientDAO.selectClientByLogin(user.getLogin());
        Product product = productDAO.selectProductByID(productDto.getId());
        checkProductAndProductDto(product, productDto);

        basketDAO.updateBasket(product, client, productDto.getCount());
        Basket basket = new Basket();
        basket.addProduct(product, productDto.getCount());
        return toBasketDto(basket);
    }

    public void deletePosition(Integer id, String token) throws ServerException {
        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkClientStatus(user);
        Client client = clientDAO.selectClientByLogin(user.getLogin());
        basketDAO.deleteBasket(id, client);
    }

    public BasketDto getBasket(String token) throws ServerException {
        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkClientStatus(user);
        Client client = clientDAO.selectClientByLogin(user.getLogin());
        Basket basket = basketDAO.selectBasket(client);
        if (basket == null) {
            BasketDto basketDto = new BasketDto();
            basketDto.setRemaining(new ArrayList<ProductDto>());
            return basketDto;
        }
        return toBasketDto(basket);
    }

    public BasketDto toBasketDto(Basket basket) {
        BasketDto basketDto = new BasketDto();

        for (Product product : basket.getListProducts()) {
            ProductDto productDto = new ProductDto();
            productDto.setCount(product.getCount());
            productDto.setName(product.getName());
            productDto.setPrice(product.getPrice());
            productDto.setId(product.getId());
            basketDto.addPurchaseDto(productDto);
        }

        return basketDto;
    }

    private void checkProductAndProductDto(Product product, ProductDto productDto) throws ServerException {
        if (product == null) throw new ServerException(ServerErrorCode.SERVER_PRODUCT_NOT_FOUND, "id");
        if (!product.getName().equals(productDto.getName()))
            throw new ServerException(ServerErrorCode.SERVER_INCORRECT_PRODUCT_REQUEST, "name");
        if (!product.getPrice().equals(productDto.getPrice()))
            throw new ServerException(ServerErrorCode.SERVER_INCORRECT_PRODUCT_REQUEST, "price");
        if (productDto.getCount() < 1)
            throw new ServerException(ServerErrorCode.SERVER_INCORRECT_PRODUCT_REQUEST, "count");
    }
}
