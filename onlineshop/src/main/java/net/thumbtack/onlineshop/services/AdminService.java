package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.dao.*;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    private AdminDAO adminDAO;
    private UserService userService;
    private SessionService sessionService;

    @Autowired
    public AdminService(AdminDAO adminDAO, UserService userService, SessionService sessionService) {
        this.adminDAO = adminDAO;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    public UserDto saveAdmin(UserDto userDto) throws ServerException {
        if (userDto.getPosition() == null) throw new ServerException(ServerErrorCode.SERVER_NULL_POSITION, "position");
        userDto.setStatus(UserStatus.ADMIN);
        User user = userService.saveUser(userDto);
        Admin admin = new Admin();
        admin.setUser(user);
        admin.setPosition(userDto.getPosition());
        adminDAO.insertAdmin(admin);
        sessionService.setActiveUser(user);
        return toUserDto(admin);
    }

    public UserDto updateAdmin(UserDto userDto, final String token) throws ServerException {
        if (userDto.getPosition() == null) throw new ServerException(ServerErrorCode.SERVER_NULL_POSITION, "position");
        User user = sessionService.getActiveUser(token);
        StatusCheckUtility.checkAdminStatus(user);

        Admin admin = new Admin();
        if (!userDto.getOldPassword().equals(user.getPassword()))
            throw new ServerException(ServerErrorCode.SERVER_INVALID_PASSWORD, "oldPassword");

        admin.setUser(userService.updateUser(user, userDto));
        admin.setPosition(userDto.getPosition());
        adminDAO.updateAdmin(admin);
        return toUserDto(admin);
    }

    private UserDto toUserDto(Admin admin) {
        UserDto userDto = new UserDto();
        userDto.setId(admin.getUser().getId());
        userDto.setFirstName(admin.getUser().getFirstName());
        userDto.setSecondName(admin.getUser().getSecondName());
        userDto.setThirdName(admin.getUser().getThirdName());
        userDto.setLogin(admin.getUser().getLogin());
        userDto.setPosition(admin.getPosition());
        return userDto;
    }

}
