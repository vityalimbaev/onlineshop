package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.dto.UserLoginDto;
import net.thumbtack.onlineshop.jdbc.dao.AdminDAO;
import net.thumbtack.onlineshop.jdbc.dao.ClientDAO;
import net.thumbtack.onlineshop.jdbc.dao.UserDAO;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class SessionService {
    private Map<String, User> mapActiveUsers;
    private UserDAO userDAO;
    private AdminDAO adminDAO;
    private ClientDAO clientDAO;

    @Autowired
    public SessionService(UserDAO userDAO, AdminDAO adminDAO, ClientDAO clientDAO) {
        this.userDAO = userDAO;
        this.adminDAO = adminDAO;
        this.clientDAO = clientDAO;
        mapActiveUsers = new HashMap<>();
    }

    public String login(UserLoginDto userLoginDto) throws ServerException {
        checkUserLoginDto(userLoginDto);
        User user = userDAO.selectUserByLogin(userLoginDto.getLogin());
        checkSession(userLoginDto, user);
        return setActiveUser(user);
    }

    public String logout(String token) throws ServerException {
        mapActiveUsers.remove(token);
        return "";
    }

    public String setActiveUser(User user) {
        UUID token = UUID.randomUUID();
        mapActiveUsers.put(token.toString(), user);
        return token.toString();
    }

    public User getActiveUser(String token) throws ServerException {
        User user = mapActiveUsers.get(token);
        if (user == null) throw new ServerException(ServerErrorCode.SERVER_ACTIVE_USER_NOT_FOUND,"");
        return user;
    }

    public String getActiveUserToken(String login) throws ServerException {
        for (Map.Entry<String, User> entry : mapActiveUsers.entrySet()) {
            if (entry.getValue().getLogin().equals(login))
                return entry.getKey();
        }
        throw new ServerException(ServerErrorCode.SERVER_ACTIVE_USER_NOT_FOUND,"");
    }

    public UserDto getProfileByToken(String token) throws ServerException {
        User user = getActiveUser(token);
        return getProfile(user.getLogin());
    }

    public UserDto getProfile(String login) throws ServerException {
        if(login == null) throw new ServerException( ServerErrorCode.SERVER_INVALID_LOGIN, "");
        User user = userDAO.selectUserByLogin(login);
        if(user == null) throw  new ServerException(ServerErrorCode.SERVER_USER_NOT_FOUND,"login");
        UserDto userDto = new UserDto();

        userDto.setFirstName(user.getFirstName());
        userDto.setSecondName(user.getSecondName());
        userDto.setThirdName(user.getThirdName());
        userDto.setLogin(user.getLogin());

        if (user.getStatus() == UserStatus.ADMIN) {
            Admin admin = adminDAO.selectAdminByLogin(login);
            userDto.setPosition(admin.getPosition());
            userDto.setId(admin.getId());
        } else {
            Client client = clientDAO.selectClientByLogin(login);
            userDto.setEmail(client.getEmail());
            userDto.setAddress(client.getAddress());
            userDto.setDeposit(client.getDeposit());
            userDto.setPhone(client.getPhone());
            userDto.setId(client.getId());
        }
        return userDto;
    }

    private void checkSession(UserLoginDto userDto, User user) throws ServerException {
        if(user == null) throw new ServerException(ServerErrorCode.SERVER_USER_NOT_FOUND, "login");
        if(!userDto.getPassword().equals(user.getPassword())) throw new ServerException(ServerErrorCode.SERVER_INVALID_PASSWORD, "password");
        if (mapActiveUsers.containsValue(user)) {
            throw new ServerException(ServerErrorCode.SERVER_ACTIVE_USER_ALREADY_EXIST, "login");
        }
    }

    private void checkUserLoginDto(UserLoginDto userLoginDto) throws ServerException {
        if(userLoginDto.getLogin() == null) throw new ServerException(ServerErrorCode.SERVER_NULL_LOGIN, "login");
        if(userLoginDto.getPassword() == null) throw new ServerException(ServerErrorCode.SERVER_NULL_PASSWORD, "password");
    }

}
