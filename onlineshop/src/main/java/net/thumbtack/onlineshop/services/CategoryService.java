package net.thumbtack.onlineshop.services;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.jdbc.dao.CategoryDAO;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class CategoryService {

    private CategoryDAO categoryDAO;
    private SessionService sessionService;

    @Autowired
    public CategoryService(CategoryDAO categoryDAO, SessionService sessionService) {
        this.sessionService = sessionService;
        this.categoryDAO = categoryDAO;
    }

    public CategoryDto saveCategory(CategoryDto categoryDto, String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        checkCategory(categoryDto);
        Category category = fromCategoryDto(categoryDto);

        if (category.getParentId() != null) {
            category.setParentName(categoryDAO
                    .selectCategoryById(category.getParentId())
                    .getName());
        }

        return toCategoryDto(categoryDAO.insertCategory(category));
    }

    public CategoryDto updateCategory(CategoryDto categoryDto, final String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        checkCategory(categoryDto);
        Category category = fromCategoryDto(categoryDto);
        categoryDAO.updateCategory(category);
        return categoryDto;
    }

    public void removeCategory(int id, final String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        categoryDAO.deleteCategory(id);
    }

    public List<CategoryDto> getAllCategories(final String token) throws ServerException {
        StatusCheckUtility.checkAdminStatus(sessionService.getActiveUser(token));
        List<Category> list = categoryDAO.selectAllCategory();
        List<CategoryDto> listCategoryDto = new ArrayList<>();

        if (list == null) return listCategoryDto;

        Collections.sort(list, new Comparator<Category>() {
            public int compare(Category one, Category other) {
                return one.getName().compareTo(other.getName());
            }
        });

        for (Category category : list) {
            listCategoryDto.add(toCategoryDto(category));
            if (category.getListCategory() != null) {
                category.sortSubCategories();
                for (Category subCategory : category.getListCategory()) {
                    listCategoryDto.add(toCategoryDto(subCategory));
                }
            }
        }
        return listCategoryDto;
    }

    public CategoryDto toCategoryDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setParentId(category.getParentId());
        return categoryDto;
    }

    public Category fromCategoryDto(CategoryDto categoryDto) {
        Category category = new Category();
        category.setId(categoryDto.getId());
        category.setParentId(categoryDto.getParentId());
        category.setName(categoryDto.getName());
        return category;
    }

    private void checkCategory(CategoryDto categoryDto) throws ServerException {
        if (categoryDto.getName() == null)
            throw new ServerException(ServerErrorCode.SERVER_INCORRECT_CATEGORY_DATA, "name");
        if (categoryDto.getParentId() != null && categoryDAO.selectCategoryById(categoryDto.getParentId()) == null) {
            throw new ServerException(ServerErrorCode.SERVER_INCORRECT_CATEGORY_DATA, "parentId");
        }
    }


}
