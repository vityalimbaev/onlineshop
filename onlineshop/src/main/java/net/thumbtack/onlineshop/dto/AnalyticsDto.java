package net.thumbtack.onlineshop.dto;

import lombok.Data;
import net.thumbtack.onlineshop.jdbc.model.Purchase;

import java.util.List;

@Data
public class AnalyticsDto {
    private Integer count;
    private Integer gain;
    private List<Purchase> purchases;
}
