package net.thumbtack.onlineshop.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PurchaseBasketDto {
    private List<ProductDto> remaining;
    private List<ProductDto> bought;

    public void addToRemaining(ProductDto productDto){
        if(remaining == null) remaining = new ArrayList<>();
        remaining.add(productDto);
    }

    public void addToBougth(ProductDto productDto){
        if(bought == null) bought = new ArrayList<>();
        bought.add(productDto);
    }
}
