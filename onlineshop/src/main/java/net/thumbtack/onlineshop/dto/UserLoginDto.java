package net.thumbtack.onlineshop.dto;

import lombok.Data;

@Data
public class UserLoginDto {
    private String login;
    private String password;
}
