package net.thumbtack.onlineshop.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class CategoryDto {
    private Integer id;
    private String name;
    private Integer parentId;
    private String parentName;
}
