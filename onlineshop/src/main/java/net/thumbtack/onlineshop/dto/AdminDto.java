package net.thumbtack.onlineshop.dto;

import lombok.Data;

@Data
public class AdminDto {
    private Integer id;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String position;
    private String login;
    private String password;
    private String oldPassword;
    private String newPassword;
}