package net.thumbtack.onlineshop.dto;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonIgnore;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class UserDto {
    private Integer id;
    private String firstName;
    private String secondName;
    private String thirdName;

    private String login;

    private String position;

    private String password;
    private String newPassword;
    private String oldPassword;

    private String phone;
    private String address;
    private String email;
    private Integer deposit;

    private UserStatus status;
}
