package net.thumbtack.onlineshop.dto;

import lombok.Data;

@Data
public class ExceptionDto {
    private String errorName;
    private String message;
    private int errorCode;
    private String field;
}
