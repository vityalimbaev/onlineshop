package net.thumbtack.onlineshop.dto;

import lombok.Data;

@Data
public class PurchaseDto {
    private Integer id;
    private String name;
    private Integer price;
    private Integer count;
}
