package net.thumbtack.onlineshop.dto;

import lombok.Data;
import net.thumbtack.onlineshop.jdbc.model.Product;

import java.util.ArrayList;
import java.util.List;

@Data
public class BasketDto {
    public List<ProductDto> remaining;
    public void addPurchaseDto(ProductDto productDto){
        if(remaining == null){
            remaining = new ArrayList<>();
        }
            remaining.add(productDto);
    }

}

