package net.thumbtack.onlineshop.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class ProductDto {
    private Integer id;
    private String name;
    private Integer price;
    private Integer count;
    private List<Integer> categoriesId;
    private List<String> categories;
}