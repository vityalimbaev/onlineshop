package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserMapper implements RowMapper<User> {
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setSecondName(resultSet.getString("second_name"));
        user.setThirdName(resultSet.getString("third_name"));
        user.setLogin(resultSet.getString("login"));
        user.setStatus(resultSet.getString("status").equals("admin") ? UserStatus.ADMIN : UserStatus.CLIENT);
        user.setPassword(resultSet.getString("password"));
        return user;
    }
}
