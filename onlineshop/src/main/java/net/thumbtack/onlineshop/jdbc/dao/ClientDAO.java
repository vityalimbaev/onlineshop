package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.ClientMapper;
import net.thumbtack.onlineshop.jdbc.model.Client;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class ClientDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private ClientMapper clientMapper;

    private final String SQL_INSERT = "INSERT INTO client (id_user, address, email, phone) VALUES " +
            "(:userId, :address, :email, :phone)";
    public final String SQL_UPDATE = "UPDATE client SET address = :address, phone = :phone WHERE id = :id";
    public final String SQL_DEPOSIT_UPDATE = "UPDATE client SET deposit = ? WHERE id = ?";
    public final String SQL_DELETE = "DELETE * FROM client WHERE id = ?";

    public final String SQL_SELECT_ALL =
            "SELECT client.id AS client_id, client.email, client.phone, client.address, client.deposit," +
                    " user.id AS user_id, user.first_name, user.second_name, user.third_name, user.login, user.password, user.status" +
                    " FROM client JOIN user ON client.id_user = user.id ";

    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE client.id = ? ";
    public final String SQL_SELECT_BY_LOGIN = SQL_SELECT_ALL + " WHERE user.login = ?";
    private final String SQL_SELECT_BY_EMAIL = SQL_SELECT_ALL + " WHERE client.email = ?";
    private final String SQL_SELECT_BY_PHONE = SQL_SELECT_ALL + " WHERE client.phone = ?";

    public ClientDAO(DataSource dataSource, ClientMapper clientMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.clientMapper = clientMapper;
    }

    public Client insertClient(Client client) {
        if (client == null) throw new IllegalArgumentException();
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", client.getUser().getId())
                .addValue("address", client.getAddress())
                .addValue("email", client.getEmail())
                .addValue("phone", client.getPhone());

        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        client.setId(keyHolder.getKey().intValue());
        return client;
    }

    public Client updateClient(Client client) {
        if (client == null) throw new IllegalArgumentException();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("address", client.getAddress())
                .addValue("phone", client.getPhone())
                .addValue("id", client.getId());

        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return client;
    }

    public Client deleteClient(Client client) {
        jdbcTemplate.update(SQL_DELETE, client.getId());
        return client;
    }

    public Integer updateDeposit(Integer deposit, int id) {
        jdbcTemplate.update(SQL_DEPOSIT_UPDATE, deposit, id);
        return deposit;
    }

    public List<Client> selectAllClient() {
        return jdbcTemplate.query(SQL_SELECT_ALL, clientMapper);
    }

    public Client selectClient(int id) {
        List<Client> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, clientMapper);
        for (Client client : list) {
            if (client.getUser().getId() == id) {
                return client;
            }
        }
        return null;
    }

    public Client selectClientByLogin(String login) {
        if (login == null) throw new IllegalArgumentException();

        List<Client> list = jdbcTemplate.query(SQL_SELECT_BY_LOGIN, new Object[]{login}, clientMapper);
        for (Client client : list) {
            if (client.getUser().getLogin().equals(login)) {
                return client;
            }
        }
        return null;
    }

    public Client selectClientByPhone(String phone) {
        if (phone == null) throw new IllegalArgumentException();

        List<Client> list = jdbcTemplate.query(SQL_SELECT_BY_PHONE, new Object[]{phone}, clientMapper);
        for (Client client : list) {
            if (client.getPhone().equals(phone)) {
                return client;
            }
        }
        return null;
    }

    public Client selectClientByEmail(String email) {
        if (email == null) throw new IllegalArgumentException();

        List<Client> list = jdbcTemplate.query(SQL_SELECT_BY_EMAIL, new Object[]{email}, clientMapper);
        for (Client client : list) {
            if (client.getEmail().equals(email)) {
                return client;
            }
        }
        return null;
    }
}
