package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.AdminMapper;
import net.thumbtack.onlineshop.jdbc.model.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class AdminDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private AdminMapper adminMapper;

    public final String SQL_INSERT = "INSERT INTO admin (id_user, position) VALUES (:idUser, :position)";
    public final String SQL_UPDATE = "UPDATE admin SET position = :position";
    public final String SQL_DELETE = "DELETE * FROM admin WHERE id = ?";

    public final String SQL_SELECT_ALL = "SELECT admin.id AS admin_id, admin.position, " +
            "user.id AS user_id, user.first_name, user.second_name, user.third_name, user.login, user.password" +
            " FROM admin JOIN user ON admin.id_user = user.id";

    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE admin.id = ?";
    public final String SQL_SELECT_BY_LOGIN = SQL_SELECT_ALL + " WHERE user.login = ?";

    @Autowired
    public AdminDAO(DataSource dataSource, AdminMapper adminMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.adminMapper = adminMapper;
    }

    public Admin insertAdmin(Admin admin) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("idUser", admin.getUser().getId())
                .addValue("position", admin.getPosition());

        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        admin.setId(keyHolder.getKey().intValue());
        return admin;
    }

    public Admin updateAdmin(Admin admin) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("idUser", admin.getUser().getId())
                .addValue("position", admin.getPosition());

        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return admin;
    }

    public Admin deleteAdmin(Admin admin) {
        jdbcTemplate.update(SQL_DELETE, admin.getId());
        return admin;
    }

    public List<Admin> selectAllAdmins() {
        return jdbcTemplate.query(this.SQL_SELECT_ALL, adminMapper);
    }

    public Admin selectAdminById(int id) {
        List<Admin> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, adminMapper);
        for (Admin admin : list) {
            if (admin.getId() == id) {
                return admin;
            }
        }
        return null;
    }

    public Admin selectAdminByLogin(String login) {
        List<Admin> list = jdbcTemplate.query(SQL_SELECT_BY_LOGIN, new Object[]{login}, adminMapper);

        for (Admin admin : list) {
            if (admin.getUser().getLogin().equals(login)) {
                return admin;
            }
        }
        return null;
    }
}
