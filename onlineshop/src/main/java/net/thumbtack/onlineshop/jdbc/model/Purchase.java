package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;

@Data
public class Purchase {
    private Integer id;
    private Integer productId;
    private String name;
    private Integer price;
    private Integer count;
    private Integer clientId;
}
