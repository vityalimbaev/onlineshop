package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.ProductMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class ProductDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private ProductMapper productMapper;

    public final String SQL_INSERT_CATEGORY = "INSERT INTO product_category (id_category, id_product) VALUES (?,?)";
    public final String SQL_DELETE_CATEGORY = "DELETE FROM product_category WHERE id_product = ?";

    public final String SQL_INSERT = "INSERT INTO product (name,count,price) VALUES (:name, :count, :price)";
    public final String SQL_UPDATE = "UPDATE product SET name = :name, price = :price, count = :count" +
            " WHERE id = :id";
    public final String SQL_DELETE = "DELETE FROM product WHERE id = ?";

    public final String SQL_SELECT_ALL = "SELECT " +
            "parent.id as parentId, " +
            "parent.name as parentName, " +
            "category.id as categoryId, " +
            "category.name as categoryName, " +
            "product.id, product.name, " +
            "product.price, " +
            "product.count " +
            "FROM product  left join product_category on product_category.id_product = product.id " +
            "left join category ON product_category.id_category = category.id " +
            "left join category parent on category.id_parent = parent.id";

    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE product.id = ?";
    public final String SQL_SELECT_BY_NAME = SQL_SELECT_ALL + " WHERE product.name = ?";
    public final String SQL_SELECT_ALL_BY_CATEGORY_ID = SQL_SELECT_ALL + " WHERE category.id = ?";
    public final String SQL_SELECT_ALL_PRODUCT_CATEGORY = "SELECT id_category from product_category";
    public final String SQL_SELECT_ALL_PRODUCT_BY_NAME_AND_PRICE = SQL_SELECT_ALL + " WHERE product.name =? AND product.price = ?";

    @Autowired
    public ProductDAO(DataSource dataSource, ProductMapper productMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.productMapper = productMapper;
    }

    public Product insertProduct(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", product.getName())
                .addValue("price", product.getPrice())
                .addValue("count", product.getCount());
        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        product.setId(keyHolder.getKey().intValue());
        return product;
    }

    public void insertProductCategories(List<Integer> categories, int id) {
        for (Integer category : categories) {
            jdbcTemplate.update(SQL_INSERT_CATEGORY, category, id);
        }
    }

    public Product updateProduct(Product product) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", product.getId())
                .addValue("name", product.getName())
                .addValue("price", product.getPrice())
                .addValue("count", product.getCount());
        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return product;
    }

    public int deleteProduct(int id) {
        jdbcTemplate.update(SQL_DELETE, id);
        return id;
    }

    public void deleteProductCategory(int id) {
        jdbcTemplate.update(SQL_DELETE_CATEGORY, id);
    }

    public List<Product> selectAllProduct() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productMapper).get(0);
    }

    public List<Product> selectAllProductByCategory(int categoryId) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_CATEGORY_ID, new Object[]{categoryId}, productMapper).get(0);
    }

    public List<Integer> selectAllProductCategory() {
        return jdbcTemplate.queryForList(SQL_SELECT_ALL_PRODUCT_CATEGORY, Integer.class);
    }

    public Product selectProductByID(int id) {
        List<List<Product>> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, productMapper);
        if (list.size() != 0) {
            for (Product product : list.get(0)) {
                if (product.getId() == id) {
                    return product;
                }
            }
        }
        return null;
    }

    public Product selectProductByName(String name) {
        if (name == null) throw new IllegalArgumentException();

        List<Product> list = jdbcTemplate.query(SQL_SELECT_BY_NAME, new Object[]{name}, productMapper).get(0);
        for (Product product : list) {
            if (product.getName().equals(name)) {
                return product;
            }
        }
        return null;
    }

    public Product selectProductByNameAndPrice(String name, int price) {
        if (name == null) throw new IllegalArgumentException();
        List<Product> list = jdbcTemplate.query(SQL_SELECT_ALL_PRODUCT_BY_NAME_AND_PRICE, new Object[]{name, price}, productMapper).get(0);
        return list.get(0);
    }
}
