package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.BasketMapper;
import net.thumbtack.onlineshop.jdbc.daoMappers.ProductMapper;
import net.thumbtack.onlineshop.jdbc.model.Basket;
import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class BasketDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private BasketMapper basketMapper;
    private ProductMapper productMapper;

    public final String SQL_INSERT_POSITION = "INSERT basket (id_product, id_client, count) VALUES (:productId, :clientId, :count)";
    public final String SQL_UPDATE_POSITION = "UPDATE basket SET count = ? WHERE id_product = ? AND id_client = ? ";
    public final String SQL_DELETE_POSITION = "DELETE FROM basket WHERE id_product = ? AND id_client = ?";
    public final String SQL_SELECT_BASKET = "SELECT product.id AS productId, product.name , product.price, basket.count, basket.id_client " +
            "FROM product JOIN basket WHERE basket.id_client = ?";
    private final String SQL_SELECT_POSITION = "SELECT id, name, price, count FROM product WHERE id = ?";

    @Autowired
    public BasketDAO(DataSource dataSource, BasketMapper basketMapper, ProductMapper productMapper) {
        this.basketMapper = basketMapper;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.productMapper = productMapper;
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public void insertBasket( Product product, Client client, Integer count) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("productId", product.getId())
                .addValue("clientId", client.getId())
                .addValue("count", count);
        namedParameterJdbcTemplate.update(SQL_INSERT_POSITION, sqlParameterSource);
    }

    public void updateBasket(Product product, Client client, Integer count) {
        jdbcTemplate.update(SQL_UPDATE_POSITION, count,product.getId(),client.getId());
    }

    public void deleteBasket(Integer id, Client client){
        jdbcTemplate.update(SQL_DELETE_POSITION, id, client.getId());
    }

    public Basket selectBasket(Client client){

        List<Basket> list = jdbcTemplate.query(SQL_SELECT_BASKET, new Object[]{client.getId()},basketMapper );
        if(list.size()== 0){
            return null;
        }
        return list.get(0);
    }

}
