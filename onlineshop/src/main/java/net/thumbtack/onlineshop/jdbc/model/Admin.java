package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Admin {
    private int id;
    private String position;
    private User user;
}
