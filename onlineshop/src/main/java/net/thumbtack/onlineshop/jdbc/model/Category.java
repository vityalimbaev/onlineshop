package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;
import org.springframework.util.comparator.ComparableComparator;

import java.util.*;

@Data
public class Category {
    private int id = 0;
    private List<Category> listCategories;
    private Integer parentId ;
    private String parentName;
    private String name;

    public List<Category> getListCategory() {
        if(listCategories != null) return listCategories;
        return null;
    }

    public void sortSubCategories(){
        if(listCategories != null) {
            Collections.sort(listCategories, new Comparator<Category>() {
                public int compare(Category one, Category other) {
                    return one.getName().compareTo(other.getName());
                }
            });
        }
    }

    public void addCategory(Category category){
        if(category == null) throw new IllegalArgumentException();
        listCategories = new ArrayList<>();
        if(!listCategories.contains(category)) listCategories.add(category);
    }

}
