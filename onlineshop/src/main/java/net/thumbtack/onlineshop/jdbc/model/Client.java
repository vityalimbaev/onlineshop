package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;

@Data
public class Client {
    private Integer id;
    private Integer deposit;
    private Basket basket;
    private User user;
    private String address;
    private String phone;
    private String email;

    public Integer addCash(int cash){
        if(deposit == null) deposit = 0;
        deposit+=cash;
        return deposit;
    }
}
