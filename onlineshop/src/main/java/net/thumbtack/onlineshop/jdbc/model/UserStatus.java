package net.thumbtack.onlineshop.jdbc.model;

public enum UserStatus {
    ADMIN("admin"),
    CLIENT("client");

    String string;
    UserStatus(String string){
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
