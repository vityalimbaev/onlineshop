package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class Product {
    private Integer id;
    private Integer price;
    private Integer count;
    private String name;
    private List<Category> categories;

    public void addCategory(Category category){
        if(categories == null) categories = new ArrayList<>();
        if(!categories.contains(category)) categories.add(category);
    }
}
