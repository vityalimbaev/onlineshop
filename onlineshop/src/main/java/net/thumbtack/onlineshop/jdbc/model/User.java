package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private UserStatus status;
    private String firstName;
    private String secondName;
    private String thirdName;
    private String login;
    private String password;
}
