package net.thumbtack.onlineshop.jdbc.dao;

import com.mysql.cj.jdbc.CallableStatement;
import net.thumbtack.onlineshop.jdbc.daoMappers.CategoryMapper;
import net.thumbtack.onlineshop.jdbc.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

@Service
public class CategoryDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CategoryMapper categoryMapper;

    public final String SQL_INSERT_SUBCATEGORY = "INSERT INTO category (name, id_parent) VALUES (:name, :parentID)";
    public final String SQL_INSERT = "INSERT INTO category (name) VALUES (:name)";

    public final String SQL_SELECT_ALL = "SELECT  parentCategory.name as parentName, category.name as name, " +
            " parentCategory.id as parentId, category.id as id " +
            "FROM category left join category parentCategory ON category.id_parent = parentCategory.id ";

    public final String SQL_UPDATE = "UPDATE category SET name = :name, id_parent = :parentID WHERE id = :id";
    public final String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE category.id = ? ";
    public final String SQL_SELECT_BY_NAME = SQL_SELECT_ALL + " WHERE name = ?";
    public final String SQL_DELETE = "DELETE FROM category WHERE id = ?";
    public final String SQL_SELECT_BY_PRODUCT = "SELECT * FROM category " +
            "JOIN product_group ON category.id = product_group.id_group where product_group.id_product = ? ";


    @Autowired
    public CategoryDAO(DataSource dataSource, CategoryMapper categoryMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.categoryMapper = categoryMapper;
    }

    public Category insertCategory(Category category) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", category.getName());
        if (category.getParentId() != null) {
            ((MapSqlParameterSource) sqlParameterSource).addValue("parentID", category.getParentId());
            namedParameterJdbcTemplate.update(SQL_INSERT_SUBCATEGORY, sqlParameterSource, keyHolder);
        } else {
            namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        }
        category.setId(keyHolder.getKey().intValue());
        return category;
    }

    public Category updateCategory(Category category) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", category.getName())
                .addValue("parentID", category.getParentId())
                .addValue("id", category.getId());
        namedParameterJdbcTemplate.update(SQL_UPDATE, sqlParameterSource);
        return category;
    }

    public List<Category> selectAllCategory() {
        List<List<Category>> list = jdbcTemplate.query(SQL_SELECT_ALL, categoryMapper);
        if (list.size() == 0) return null;
        return list.get(0);

    }


    public Category selectCategoryById(int id) {
        List<Category> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, categoryMapper).get(0);
        if (!list.isEmpty()) {
            for (Category category : list) {
                if (category.getId() == id) {
                    return category;
                }
            }
        }
        return null;
    }

    public Category selectCategoryByName(String name) {
        if (name == null) throw new IllegalArgumentException();
        List<Category> list = jdbcTemplate.query(SQL_SELECT_BY_NAME, new Object[]{name}, categoryMapper).get(0);
        for (Category category : list) {
            if (category.getName().equals(name)) {
                return category;
            }
        }
        return null;
    }

    public List<Category> selectCategoryByProduct(int id) {
        List<List<Category>> list = jdbcTemplate.query(SQL_SELECT_BY_PRODUCT, new Object[]{id}, categoryMapper);
        return list.get(0);
    }

    public void deleteCategory(int id) {
        jdbcTemplate.update(SQL_DELETE, id);
    }
}
