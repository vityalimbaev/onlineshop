package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Purchase;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PurchaseMapper implements RowMapper<Purchase> {
    @Override
    public Purchase mapRow(ResultSet resultSet, int i) throws SQLException {
        Purchase purchase = new Purchase();
        purchase.setName(resultSet.getString("name"));
        purchase.setId(resultSet.getInt("id"));
        purchase.setCount(resultSet.getInt("count"));
        purchase.setPrice(resultSet.getInt("price"));
        purchase.setClientId(resultSet.getInt("id_client"));
        purchase.setProductId(resultSet.getInt("id_product"));

        return purchase;
    }
}
