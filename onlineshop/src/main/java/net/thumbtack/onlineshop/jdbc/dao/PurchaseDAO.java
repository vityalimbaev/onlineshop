package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.PurchaseMapper;
import net.thumbtack.onlineshop.jdbc.model.Purchase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class PurchaseDAO {
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private PurchaseMapper purchaseMapper;

    public final String SQL_INSERT = "INSERT INTO purchase (id_product, name, count , price, id_client) " +
            "VALUES (:productId, :name, :count, :price, :clientId)";
    public final String SQL_SELECT_ALL = "SELECT * FROM purchase";
    public final String SQL_SELECT_ALL_BY_CLIENT = SQL_SELECT_ALL + " WHERE id_client = ?";
    public final String SQL_SELECT_ALL_BY_NAME = SQL_SELECT_ALL + " WHERE name = ?";

    @Autowired
    public PurchaseDAO(DataSource dataSource, PurchaseMapper purchaseMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.purchaseMapper = purchaseMapper;
    }

    public Purchase insertPurchase(Purchase purchase) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("productId", purchase.getProductId())
                .addValue("name", purchase.getName())
                .addValue("count", purchase.getCount())
                .addValue("price", purchase.getPrice())
                .addValue("clientId", purchase.getClientId());
        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, keyHolder);
        purchase.setId(keyHolder.getKey().intValue());
        return purchase;
    }

    public List<Purchase> selectPurchasesByClient(int id) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_CLIENT, new Object[]{id}, purchaseMapper);
    }

    public List<Purchase> selectPurchasesByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_NAME, new Object[]{name}, purchaseMapper);
    }

    public List<Purchase> selectAllPurchases() {
        return jdbcTemplate.query(SQL_SELECT_ALL, purchaseMapper);
    }
}
