package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Component
public class CategoryMapper implements RowMapper<List<Category>> {

    @Override
    public List<Category> mapRow(ResultSet resultSet, int i) throws SQLException {
        Map<Integer, Category> categories = new HashMap<>();

        do{
            Category category = createCategory(resultSet.getString("name"), resultSet.getInt("id"));
            if(resultSet.getString("parentId") != null){
                category.setParentId(resultSet.getInt("parentId"));
                category.setParentName(resultSet.getString("parentName"));
                categories.put(category.getId(),category);
            }else{
                categories.put(category.getId(), category);
            }
        }while (resultSet.next());

        return new ArrayList<>(categories.values());
    }

    public Category createCategory(String name, int id) {
        Category category = new Category();
        category.setId(id);
        category.setName(name);
        return category;
    }
}
