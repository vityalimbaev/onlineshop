package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Client;
import net.thumbtack.onlineshop.jdbc.model.Product;
import net.thumbtack.onlineshop.jdbc.model.User;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ClientMapper implements RowMapper<Client> {

    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
        Client client = getClient(resultSet);
        client.setUser(getUser(resultSet));
        return client;
    }

    private Client getClient(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setId(resultSet.getInt("client_id"));
        client.setAddress(resultSet.getString("address"));
        client.setDeposit(resultSet.getInt("deposit"));
        client.setEmail(resultSet.getString("email"));
        client.setPhone(resultSet.getString("phone"));
        return client;
    }

    private User getUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("user_id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setSecondName(resultSet.getString("second_name"));
        user.setThirdName(resultSet.getString("third_name"));
        user.setLogin(resultSet.getString("login"));
        user.setPassword(resultSet.getString("password"));
        user.setStatus(resultSet.getString("status").equals("client") ? UserStatus.CLIENT : UserStatus.ADMIN);
        return user;
    }

}
