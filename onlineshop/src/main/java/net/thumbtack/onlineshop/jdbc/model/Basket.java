package net.thumbtack.onlineshop.jdbc.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Basket {
    private List<Product> listProducts;
    private Integer clientId;

    public void addProduct(Product product, int count){
        product.setCount(count);
        if(listProducts == null) listProducts = new ArrayList<>();
        listProducts.add(product);
    }

    public Product getProductByID(int id){
        if(listProducts == null) return null;
        for (Product product : listProducts){
            if(product.getId() == id){
                return product;
            }
        }
        return null;
    }

    public void removeProduct(Product product){
       if(listProducts != null) {
           listProducts.remove(product);
       }
    }

}
