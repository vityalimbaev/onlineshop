package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Basket;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BasketMapper implements RowMapper<Basket> {

    @Override
    public Basket mapRow(ResultSet resultSet, int i) throws SQLException {
        Basket basket = new Basket();
        basket.setClientId(resultSet.getInt("id_client"));

        do {
            Product product = new Product();
            product.setPrice(resultSet.getInt("price"));
            product.setName(resultSet.getString("name"));
            product.setId(resultSet.getInt("productId"));
            basket.addProduct(product, resultSet.getInt("count"));
        } while (resultSet.next());

        return basket;
    }
}
