package net.thumbtack.onlineshop.jdbc.daoMappers;

import net.thumbtack.onlineshop.jdbc.model.Category;
import net.thumbtack.onlineshop.jdbc.model.Product;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProductMapper implements RowMapper<List<Product>> {

    public List<Product> mapRow(ResultSet resultSet, int i) throws SQLException {

        Map<Integer, Product> productMap = new HashMap<>();

        do {
            Product product = null;
            if (!productMap.containsKey(resultSet.getInt("id"))) {
                product = new Product();
                product.setId(resultSet.getInt("id"));

                product.setName(resultSet.getString("name"));
                product.setCount(resultSet.getInt("count"));
                product.setPrice(resultSet.getInt("price"));
                productMap.put(product.getId(), product);
            } else {
                product = productMap.get(resultSet.getInt("id"));
            }

            if (resultSet.getString("categoryId") != null) {
                Category category = new Category();
                category.setName(resultSet.getString("categoryName"));
                category.setId(resultSet.getInt("categoryId"));
                if (resultSet.getString("parentId") != null) {
                    category.setParentId(resultSet.getInt("parentId"));
                    category.setParentName(resultSet.getString("parentName"));
                }
                product.addCategory(category);
            }
        } while (resultSet.next());

        return new ArrayList<>(productMap.values());
    }
}
