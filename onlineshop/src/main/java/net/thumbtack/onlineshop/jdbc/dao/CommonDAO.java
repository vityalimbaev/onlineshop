package net.thumbtack.onlineshop.jdbc.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class CommonDAO {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CommonDAO(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void clearDataBase() {
        jdbcTemplate.update("DELETE FROM admin");
        jdbcTemplate.update("DELETE FROM client");
        jdbcTemplate.update("DELETE FROM product");
        jdbcTemplate.update("DELETE FROM basket");
        jdbcTemplate.update("DELETE FROM category");
        jdbcTemplate.update("DELETE FROM user");
        jdbcTemplate.update("DELETE FROM purchase");
        jdbcTemplate.update(" ALTER TABLE user AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE admin AUTO_INCREMENT= 1");
        jdbcTemplate.update(" ALTER TABLE client AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE category AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE product AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE product_category AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE purchase AUTO_INCREMENT = 1");
        jdbcTemplate.update(" ALTER TABLE basket AUTO_INCREMENT = 1");

    }
}
