package net.thumbtack.onlineshop.jdbc.dao;

import net.thumbtack.onlineshop.jdbc.daoMappers.UserMapper;
import net.thumbtack.onlineshop.jdbc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@Service
public class UserDAO {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private UserMapper userMapper;

    public final String SQL_INSERT =
            "INSERT INTO user(first_name, second_name, third_name, login, password, status) VALUES" +
                    "(:name1, :name2, :name3, :login, :password, :status)";
    public final String SQL_UPDATE =
            "UPDATE user SET first_name = :name1, second_name = :name2," +
                    "third_name = :name3, password = :password WHERE id = :id";
    public final String SQL_DELETE =
            "DELETE FROM user WHERE id = ?";
    public final String SQL_SELECT_ALL =
            "SELECT * FROM user";
    public final String SQL_SELECT_BY_ID =
            "SELECT * FROM user WHERE id = ?";
    public final String SQL_SELECT_BY_LOGGER =
            "SELECT * FROM user WHERE login = ?";

    @Autowired
    public UserDAO(DataSource dataSource, UserMapper userMapper) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.userMapper = userMapper;
    }

    public User insertUser(final User user) {
        if (user == null) throw new IllegalArgumentException();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name1", user.getFirstName())
                .addValue("name2", user.getSecondName())
                .addValue("name3", user.getThirdName())
                .addValue("login", user.getLogin())
                .addValue("password", user.getPassword())
                .addValue("status", user.getStatus().getString());

        namedParameterJdbcTemplate.update(SQL_INSERT, parameters, keyHolder);
        user.setId(keyHolder.getKey().intValue());
        return user;
    }

    public User updateUser(User user) {
        if (user == null) throw new IllegalArgumentException();

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("name1", user.getFirstName())
                .addValue("name2", user.getSecondName())
                .addValue("name3", user.getThirdName())
                .addValue("password", user.getPassword())
                .addValue("id", user.getId());

        namedParameterJdbcTemplate.update(SQL_UPDATE, parameters);
        return user;
    }

    public User deleteUser(User user) {
        if (user == null) throw new IllegalArgumentException();
        jdbcTemplate.update(SQL_DELETE, user.getId());
        return user;
    }

    public List<User> selectAllUser() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userMapper);
    }

    public User selectUser(int id) {
        List<User> list = jdbcTemplate.query(SQL_SELECT_BY_ID, new Object[]{id}, userMapper);
        for (User user : list) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public User selectUserByLogin(String login) {
        List<User> list = jdbcTemplate.query(SQL_SELECT_BY_LOGGER, new Object[]{login}, userMapper);
        for (User user : list) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }
}
