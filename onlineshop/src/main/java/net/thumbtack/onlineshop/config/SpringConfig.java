package net.thumbtack.onlineshop.config;

import net.thumbtack.onlineshop.services.ValidationService;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@ComponentScan("net.thumbtack")
@Configuration
@EnableWebMvc
@PropertySource("classpath:config.properties")
public class SpringConfig {
    @Autowired
    private Environment env;

    @Bean(name = "datSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/online_shop?useUnicode=yes&characterEncoding=UTF8&useSSL=false&serverTimezone=Asia/Omsk");
        dataSource.setUsername("test");
        dataSource.setPassword("test");

        ValidationService.setMaxNameLength( Integer.valueOf(env.getProperty("max_name_length")));
        ValidationService.setMinPasswordLength(Integer.valueOf(env.getProperty("min_password_length")));

        return dataSource;
    }

}

