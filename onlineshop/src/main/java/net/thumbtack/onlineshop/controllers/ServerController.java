package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.jdbc.dao.CommonDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerController {

    private CommonDAO commonDAO;

    @Autowired
    private ServerController(CommonDAO commonDAO) {
        this.commonDAO = commonDAO;
    }

    @PostMapping(path = "/api/debug/clear")
    public void clearDataBase() {
        commonDAO.clearDataBase();
    }

}
