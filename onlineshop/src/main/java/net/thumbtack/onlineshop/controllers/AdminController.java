package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AdminController {

    private final String cookieName = "JAVASESSIONID";
    private AdminService adminService;
    private SessionController sessionController;

    @Autowired
    public AdminController(AdminService adminService, SessionController sessionController) {
        this.adminService = adminService;
        this.sessionController = sessionController;
    }

    @ResponseBody
    @PostMapping(path = "/api/admins", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto saveAdmin(@RequestBody UserDto userDto, HttpServletResponse httpResponse) throws ServerException {
        userDto = adminService.saveAdmin(userDto);
        httpResponse.addCookie(new Cookie(cookieName, sessionController.getSessionService().getActiveUserToken(userDto.getLogin())));
        return userDto;
    }

    @ResponseBody
    @PutMapping(path = "/api/admins", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDto updateAdmin(@RequestBody UserDto userDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return adminService.updateAdmin(userDto, token);
    }

    @ResponseBody
    @GetMapping(path = "/api/admins")
    public UserDto getAdminProfile(HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return sessionController.getSessionService().getProfileByToken(token);
    }

    public AdminService getAdminService() {
        return adminService;
    }
}
