package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.CategoryDto;
import net.thumbtack.onlineshop.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import java.util.List;

@RestController
public class CategoryController {

    private CategoryService categoryService;
    private SessionController sessionController;

    @Autowired
    public CategoryController(CategoryService categoryService, SessionController sessionController) {
        this.categoryService = categoryService;
        this.sessionController = sessionController;
    }

    @ResponseBody
    @PostMapping(path = "/api/categories", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CategoryDto addCategory(@RequestBody CategoryDto categoryDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return categoryService.saveCategory(categoryDto, token);
    }

    @ResponseBody
    @PutMapping(path = "/api/categories/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CategoryDto updateCategory(@PathVariable int id, @RequestBody CategoryDto categoryDto, HttpServletRequest request) throws ServerException {
        categoryDto.setId(id);
        String token = sessionController.getJavaSessionId(request);
        return categoryService.updateCategory(categoryDto, token);
    }

    @DeleteMapping(path = "/api/categories/{id}")
    public String deleteCategory(@PathVariable int id, HttpServletRequest request) throws ServerException {
        String token = sessionController.getJavaSessionId(request);
        categoryService.removeCategory(id, token);
        return "";
    }

    @ResponseBody
    @GetMapping(path = "/api/categories")
    public List<CategoryDto> selectCategories(HttpServletRequest request) throws ServerException {
        String token = sessionController.getJavaSessionId(request);
        return categoryService.getAllCategories(token);
    }
}
