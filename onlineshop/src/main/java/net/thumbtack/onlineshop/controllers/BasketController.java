package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.BasketDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
public class BasketController {

    private SessionController sessionController;
    private BasketService basketService;

    @Autowired
    public BasketController(SessionController sessionController, BasketService basketService) {
        this.sessionController = sessionController;
        this.basketService = basketService;
    }

    @ResponseBody
    @PostMapping(path = "/api/baskets", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BasketDto savePosition(@RequestBody ProductDto productDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return basketService.savePosition(productDto, token);
    }

    @ResponseBody
    @PutMapping(path = "/api/baskets", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BasketDto updatePosition(@RequestBody ProductDto productDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return basketService.updatePosition(productDto, token);
    }

    @ResponseBody
    @GetMapping(path = "/api/baskets")
    public BasketDto getBasket(HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return basketService.getBasket(token);
    }

    @ResponseBody
    @DeleteMapping(path = "/api/baskets/{id}")
    public BasketDto removePosition(@PathVariable int id, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        basketService.deletePosition(id, token);
        return null;
    }


}
