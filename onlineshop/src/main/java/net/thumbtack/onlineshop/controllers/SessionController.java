package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerErrorCode;
import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.dto.UserLoginDto;
import net.thumbtack.onlineshop.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class SessionController {
    private final String cookieName = "JAVASESSIONID";
    private final SessionService sessionService;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @ResponseBody
    @PostMapping(path = "/api/sessions", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDto login(@RequestBody UserLoginDto userLoginDto, HttpServletResponse httpServletResponse) throws IOException, ServerException {
        final String token = sessionService.login(userLoginDto);
        httpServletResponse.addCookie(new Cookie(cookieName, token));
        return sessionService.getProfile(userLoginDto.getLogin());
    }

    @ResponseBody
    @DeleteMapping(path = "/api/sessions")
    public String logout(HttpServletRequest httpServletRequest) throws ServerException {
        return sessionService.logout(getJavaSessionId(httpServletRequest));
    }

    public String getJavaSessionId(HttpServletRequest httpRequest) throws ServerException {
        if (httpRequest.getCookies() == null)
            throw new ServerException(ServerErrorCode.SERVER_ACTIVE_USER_NOT_FOUND, "");
        for (Cookie cookie : httpRequest.getCookies()) {
            if (cookie.getName().equals(cookieName)) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public SessionService getSessionService() {
        return sessionService;
    }

}
