package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.AnalyticsDto;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.dto.PurchaseBasketDto;
import net.thumbtack.onlineshop.services.PurchaseAnalytics;
import net.thumbtack.onlineshop.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class PurchaseController {

    private SessionController sessionController;
    private PurchaseService purchaseService;
    private PurchaseAnalytics purchaseAnalytics;

    @Autowired
    public PurchaseController(SessionController sessionController, PurchaseService purchaseService, PurchaseAnalytics purchaseAnalytics) {
        this.sessionController = sessionController;
        this.purchaseService = purchaseService;
        this.purchaseAnalytics = purchaseAnalytics;
    }

    @ResponseBody
    @PostMapping(path = "/api/purchases", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProductDto purchase(@RequestBody ProductDto productDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return purchaseService.purchaseProduct(productDto, token);
    }

    @ResponseBody
    @PostMapping(path = "/api/purchases/baskets", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PurchaseBasketDto purchase(@RequestBody List<ProductDto> listProductDto, HttpServletRequest httpServletRequest) throws ServerException, Exception {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return purchaseService.purchaseBasket(listProductDto, token);
    }

    @ResponseBody
    @GetMapping(path = "/api/purchases")
    public AnalyticsDto getList(@RequestParam(value = "categories", required = false) int[] categories,
                                @RequestParam(value = "clients", required = false) int[] clients,
                                @RequestParam(value = "name", required = false) String[] names,
                                @RequestParam(value = "sort", required = false, defaultValue = "name") String sort,
                                @RequestParam(value = "direction", required = false, defaultValue = "up") String direction,
                                @RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
                                @RequestParam(value = "limit", required = false, defaultValue = "0") int limit,
                                HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return purchaseAnalytics.getPurchaseAnalytics(clients, names, categories, sort, direction, offset, limit, token);
    }
}

