package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ProductDto;
import net.thumbtack.onlineshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.util.List;

@RestController
public class ProductController {
    private ProductService productService;
    private SessionController sessionController;

    @Autowired
    public ProductController(ProductService productService, SessionController sessionController) {
        this.productService = productService;
        this.sessionController = sessionController;
    }

    @ResponseBody
    @PostMapping(path = "/api/products", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProductDto addProduct(@RequestBody ProductDto productDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return productService.saveProduct(productDto, token);
    }

    @ResponseBody
    @PutMapping(path = "/api/products/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ProductDto updateProduct(@PathVariable int id, @RequestBody ProductDto productDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        productDto.setId(id);
        return productService.updateProduct(productDto, token);
    }

    @ResponseBody
    @DeleteMapping(path = "/api/products/{id}")
    public String deleteProduct(@PathVariable int id, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        productService.removeProduct(id, token);
        return "";
    }

    @ResponseBody
    @GetMapping(path = "/api/products/{id}")
    public ProductDto getProduct(@PathVariable int id, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return productService.getProduct(id, token);

    }

    @ResponseBody
    @GetMapping(path = "/api/products")
    public List<ProductDto> getAllProducts(
            @RequestParam(value = "order", required = false, defaultValue = "product") String order,
            @RequestParam(value = "categories", required = false) int[] categories,
            HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return productService.getAllProducts(categories, order, token);
    }

}
