package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.ExceptionDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = {ServerException.class})
    protected ResponseEntity<Object> handleConflict(ServerException ex, WebRequest request) {
        String bodyOfResponse = ex.getErrorCode().getErrorString();
        ExceptionDto exDto = new ExceptionDto();
        exDto.setErrorCode(ex.getErrorCode().getHttpStatus().value());
        exDto.setErrorName(ex.getErrorCode().name());
        exDto.setMessage(ex.getErrorCode().getErrorString());
        exDto.setField(ex.getErrorCode().getField());
        return handleExceptionInternal(ex, exDto, new HttpHeaders(), ex.getErrorCode().getHttpStatus(), request);
    }

}
