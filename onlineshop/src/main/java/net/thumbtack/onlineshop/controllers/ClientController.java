package net.thumbtack.onlineshop.controllers;

import net.thumbtack.onlineshop.ServerExceptions.ServerException;
import net.thumbtack.onlineshop.dto.UserDto;
import net.thumbtack.onlineshop.jdbc.model.UserStatus;
import net.thumbtack.onlineshop.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.List;

@RestController
public class ClientController {

    private final String cookieName = "JAVASESSIONID";
    private ClientService clientService;
    private SessionController sessionController;

    @Autowired
    public ClientController(ClientService clientService, SessionController sessionController) {
        this.sessionController = sessionController;
        this.clientService = clientService;
    }

    @ResponseBody
    @PostMapping(path = "/api/clients", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDto saveClient(@RequestBody UserDto userDto, HttpServletResponse httpServletResponse) throws ServerException {
        userDto.setStatus(UserStatus.ADMIN);
        UserDto response = clientService.saveClient(userDto);
        httpServletResponse.addCookie(
                new Cookie(cookieName, sessionController.getSessionService().getActiveUserToken(response.getLogin()))
        );
        return response;
    }

    @ResponseBody
    @PutMapping(path = "/api/clients", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDto updateClient(@RequestBody UserDto userDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return clientService.updateClient(userDto, token);
    }

    @ResponseBody
    @PostMapping(path = "/api/deposits", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserDto putCash(@RequestBody UserDto userDto, HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return clientService.putCash(userDto.getDeposit(), token);
    }

    @ResponseBody
    @GetMapping(path = "/api/clients")
    public UserDto getClient(HttpServletRequest httpServletRequest) throws ServerException {
        String token = sessionController.getJavaSessionId(httpServletRequest);
        return sessionController.getSessionService().getProfileByToken(token);
    }

}
