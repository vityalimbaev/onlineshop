package net.thumbtack.onlineshop.ServerExceptions;

import com.fasterxml.jackson.annotation.JsonInclude;
import net.thumbtack.onlineshop.services.ValidationService;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public enum ServerErrorCode {

    SERVER_USER_NOT_FOUND(HttpStatus.NOT_FOUND, "User not found"),
    SERVER_ACCESS_DENIED(HttpStatus.BAD_REQUEST, "Administrator rights are required"),
    SERVER_NULL_NAME(HttpStatus.BAD_REQUEST, "You must enter your first and last name"),
    SERVER_INVALID_NAME(HttpStatus.BAD_REQUEST, "The name must contain only Cyrillic characters, a dash and a length of no more than "
            + ValidationService.getMaxNameLength()),
    SERVER_NULL_LOGIN(HttpStatus.BAD_REQUEST, "Enter the username"),
    SERVER_INVALID_LOGIN(HttpStatus.BAD_REQUEST, "Login must contain only Latin characters and numbers and a length of no more than "
            + ValidationService.getMaxNameLength()),
    SERVER_NULL_PASSWORD(HttpStatus.BAD_REQUEST, "Enter the password"),
    SERVER_INVALID_PASSWORD(HttpStatus.BAD_REQUEST, "Password must contain only Latin characters and nambers and a length of no less than "
            + ValidationService.getMinPasswordLength()),
    SERVER_LOGIN_ALREADY_EXIST(HttpStatus.BAD_REQUEST, "Login already exist"),
    SERVER_INVALID_EMAIL(HttpStatus.BAD_REQUEST, "Invalid email"),
    SERVER_INVALID_PHONE(HttpStatus.BAD_REQUEST, "Invalid phone"),
    SERVER_PRODUCT_NOT_FOUND(HttpStatus.NOT_FOUND, "Product not found"),
    SERVER_INCORRECT_PRODUCT_REQUEST(HttpStatus.BAD_REQUEST, "Incorrect product data"),
    SERVER_INCORRECT_CATEGORY_DATA(HttpStatus.BAD_REQUEST, "Incorrect ctegory data"),
    SERVER_INVALID_ADDRESS(HttpStatus.BAD_REQUEST, "Invalid address"),
    SERVER_INCORRECT_DEPOSIT(HttpStatus.BAD_REQUEST, "Incorrect deposit, the deposit amount must be greater than 1"),
    SERVER_INVALID_PRODUCT_DATA(HttpStatus.BAD_REQUEST, "Invalid product data"),
    SERVER_CATEGORY_NOT_FOUND(HttpStatus.NOT_FOUND, "Category not found"),
    SERVER_ACTIVE_USER_ALREADY_EXIST(HttpStatus.BAD_REQUEST, "User already logged in"),
    SERVER_ACTIVE_USER_NOT_FOUND(HttpStatus.BAD_REQUEST, "Active user not found"),
    SERVER_CLIENT_DEPOSIT(HttpStatus.BAD_REQUEST, "Not enough money"),
    SERVER_NULL_POSITION(HttpStatus.BAD_REQUEST, "Enter position");

    public String getField() {
        return field;
    }

    private String field;
    private String errorString;
    private HttpStatus httpStatus;

    ServerErrorCode(HttpStatus httpStatus, String str) {
        errorString = str;
        this.httpStatus = httpStatus;
    }

    public String getErrorString() {
        return errorString;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setNameField(String string) {
        field = string;
    }
}
