package net.thumbtack.onlineshop.ServerExceptions;

public class ServerException extends Exception {

    public ServerErrorCode serverErrorCode;

    public ServerException(ServerErrorCode serverErrorCode, String field){
        serverErrorCode.setNameField(field);
        this.serverErrorCode = serverErrorCode;
    }

    public ServerErrorCode getErrorCode(){
        return serverErrorCode;
    }

}
