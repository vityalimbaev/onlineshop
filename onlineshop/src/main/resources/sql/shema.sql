DROP DATABASE IF EXISTS online_shop;
CREATE DATABASE `online_shop`;
USE `online_shop`;


CREATE TABLE user (
	id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
	second_name VARCHAR(255) NOT NULL,
    third_name  VARCHAR(255),
    status VARCHAR(10) NOT NULL,
    login VARCHAR(255)  NOT NULL unique,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
) CHARACTER SET utf8;

CREATE TABLE admin (
	id INT NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    position VARCHAR(255) NOT NULL,
	PRIMARY KEY(id),
    FOREIGN KEY (id_user) REFERENCES user (id)  ON DELETE CASCADE
) CHARACTER SET utf8;

CREATE TABLE client (
	id INT NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    address VARCHAR(255),
    email VARCHAR(255) NOT NULL unique,
    phone VARCHAR(255),
    deposit INT default 0,
    FOREIGN KEY (id_user) REFERENCES user (id) ON DELETE CASCADE, 
	PRIMARY KEY(id)
) CHARACTER SET utf8;

CREATE TABLE category(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL UNIQUE,
	id_parent INT,
    PRIMARY KEY(id),
    FOREIGN KEY (id_parent) REFERENCES category (id) ON DELETE SET NULL
) CHARACTER SET utf8;

CREATE TABLE product (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) unique,
    price INT ,
    count INT ,
    PRIMARY KEY (id)
) CHARACTER SET utf8;

CREATE TABLE product_category(
	id_product INT NOT NULL,
    id_category INT NOT NULL,
    FOREIGN KEY (id_product) REFERENCES product (id) ON DELETE CASCADE,
    FOREIGN KEY (id_category) REFERENCES category (id) ON DELETE CASCADE
) CHARACTER SET utf8;
ALTER TABLE product_category ADD CONSTRAINT uc_name UNIQUE (id_product,id_Category);

CREATE TABLE basket(
	id_product INT,
    id_client INT ,
    count INT,
    FOREIGN KEY (id_product) REFERENCES product (id) on delete cascade,
    FOREIGN KEY (id_client) REFERENCES client (id) on delete cascade
);

CREATE TABLE purchase (
	id INT  NOT NULL AUTO_INCREMENT,
    id_product INT,
    name VARCHAR(255) NOT NULL,
    price INT NOT NULL,
    count INT default 1,
    id_client INT,
    PRIMARY KEY(id),
    FOREIGN KEY (id_client) REFERENCES client (id) ON delete cascade
);